﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Models.Menu
{
    public class MenuViewModel : BaseViewModel
    {
        public MenuViewModel()
        {
            InfoMenu = new MenuModel();
            ListMenus = new List<MenuModel>();
        }
        public MenuModel InfoMenu { get; set; }
        public List<MenuModel> ListMenus { get; set; }
    }
}
