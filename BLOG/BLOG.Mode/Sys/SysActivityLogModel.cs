﻿namespace BLOG.Models.Sys
{
    public class SysActivityLogModel :BaseModel
    {
        public string LogType { get; set; }
        public string SessionId { get; set; }
        public string Comment { get; set; }
        public string ResourceType { get; set; }
        public int? UserId { get; set; }

        //Extend
        public string UserName { get; set; }
        public string CommentShort { get; set; }
    }
}
