﻿using System.ComponentModel.DataAnnotations;

namespace BLOG.Models.Sys
{
    public class SysFunctionsModel:BaseModel
    {
        public string Key { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required]
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public int? OrderBy { get; set; }
        public bool? ShowOnMenu { get; set; }
        public int? Status { get; set; }
        public int? MenuGroupId { get; set; }
        public string Icon { get; set; }
    }
}
