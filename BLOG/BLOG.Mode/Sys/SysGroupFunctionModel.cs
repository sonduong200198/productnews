﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLOG.Models.Sys
{
    public class SysGroupFunctionModel:BaseModel
    {
        public SysGroupFunctionModel()
        {
            FunctionIds = new List<int>();
        }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        //Extend
        public int TotalUsers { get; set; }
        public bool IsChecked { get; set; }

        public List<int> FunctionIds { get; set; }
    }
}
