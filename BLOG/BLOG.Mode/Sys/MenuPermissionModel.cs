﻿using System.Collections.Generic;

namespace BLOG.Models.Sys
{
    public class MenuPermissionModel
    {
        public MenuPermissionModel()
        {
            MenuItems = new List<MenuItemModel>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public List<MenuItemModel> MenuItems { get; set; }
    }

    public class MenuItemModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public int OrderBy { get; set; }
    }
}
