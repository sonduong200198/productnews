﻿using System.ComponentModel.DataAnnotations;

namespace BLOG.Models.Sys
{
    public class SysMenuGroupModel:BaseModel
    {
        [Required]
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }
    }
}
