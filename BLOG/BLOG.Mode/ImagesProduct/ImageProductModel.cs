﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Models.ImagesProduct
{
    public class ImageProductModel :BaseModel
    {
        public string Image { get; set; }
        public int ProductId { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }
}
