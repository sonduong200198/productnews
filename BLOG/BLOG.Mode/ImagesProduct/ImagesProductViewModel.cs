﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Models.ImagesProduct
{
    public class ImagesProductViewModel
    {
        public ImagesProductViewModel()
        {
            InfoImagesProduct = new ImageProductModel();
            ListImagesProduct = new List<ImageProductModel>();
        }
        public ImageProductModel InfoImagesProduct { get; set; }
        public List<ImageProductModel> ListImagesProduct { get; set; }
    }
}
