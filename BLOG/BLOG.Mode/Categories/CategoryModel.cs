﻿using BLOG.Models;
using System.ComponentModel.DataAnnotations;

namespace BLOG.Models.Categories
{
    public class CategoryModel :BaseModel
    {
        public string Name { get; set; }
        [Required]
        public string Title { get; set; }
        public string SlugUrl { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }
        public string NameParent { get; set; }
        public int OrderBy { get; set; }
        public string Status { get; set; }
        public string KeyName { get; set; }

    }
}
