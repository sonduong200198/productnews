﻿using BLOG.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Models.Categories
{
    public class CategoryViewModel :BaseViewModel
    {
        public CategoryViewModel()
        {
            ListCategory = new List<CategoryModel>();
            InfoCategory = new CategoryModel();
            CategoriesParent = new List<SelectListItem>();
        }
        public List<CategoryModel> ListCategory { get; set; }
        public CategoryModel InfoCategory { get; set; }
        public List<SelectListItem> CategoriesParent { get; set; }

    }
}
