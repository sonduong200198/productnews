﻿using BLOG.Models;
using System.ComponentModel.DataAnnotations;

namespace BLOG.Models.Posts
{
    public class TagModel :BaseModel
    {
        [Required]
        public string Name { get; set; }

    }
}
