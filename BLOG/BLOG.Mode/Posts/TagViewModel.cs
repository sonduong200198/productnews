﻿using BLOG.Models;
using System.Collections.Generic;

namespace BLOG.Models.Posts
{
    public class TagViewModel :BaseViewModel
    {
        public TagViewModel()
        {
            ListTags = new List<TagModel>();
            InfoTag = new TagModel();
        }
        public List<TagModel> ListTags { get; set; }
        public TagModel InfoTag { get; set; }
    }
}
