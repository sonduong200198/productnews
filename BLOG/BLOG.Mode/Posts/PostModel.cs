﻿using BLOG.Models;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace BLOG.Models.Posts
{
    public class PostModel :BaseModel
    {
        public string SlugUrl { get; set; }
        public string Name { get; set; }
        [Required]
        public int? CateId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public string Contents { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string Tags { get; set; }
        public string FaceIds { get; set; }
        public string[] ListTag { get; set; }
        public string[] ListFaceId { get; set; }
        public string NameCate { get; set; }
        public IFormFile Photo { get; set; }
        public string Link { get; set; }
    }
}
