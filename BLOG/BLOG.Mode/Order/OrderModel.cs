﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLOG.Models.Order
{
    public class OrderModel : BaseModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public string Type { get; set; }
        [Required]
        public string Phone { get; set; }
        public string ProductIds { get; set; }
        public string Note { get; set; }
    }
}
