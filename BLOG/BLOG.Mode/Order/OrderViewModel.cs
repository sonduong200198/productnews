﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Models.Order
{
    public class OrderViewModel : BaseViewModel
    {
        public OrderViewModel()
        {
            InfoOrder = new OrderModel();
            ListOrder = new List<OrderModel>();
        }

        public OrderModel InfoOrder { get; set; }
        public List<OrderModel> ListOrder { get; set; }
    }

}
