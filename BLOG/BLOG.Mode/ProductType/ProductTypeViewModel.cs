﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Models.ProductType
{
    public class ProductTypeViewModel
    {
        public ProductTypeViewModel()
        {
            InfoProductType = new ProductTypeModel();
            ListProductTypes = new List<ProductTypeModel>();
        }
        public ProductTypeModel InfoProductType { get; set; }
        public List<ProductTypeModel> ListProductTypes { get; set; }
    }
}
