﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Models.ProductType
{
    public class ProductTypeModel
    {
        public int Id { get; set; }
        public int? Price { get; set; }
        public int? Promotion { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public int? ProductId { get; set; }
    }
}
