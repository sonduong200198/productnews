﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BLOG.Models.Entities
{
    public partial class BLOGContext : DbContext
    {
        public BLOGContext()
        {
        }

        public BLOGContext(DbContextOptions<BLOGContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<ConstantProductType> ConstantProductType { get; set; }
        public virtual DbSet<ImagesProduct> ImagesProduct { get; set; }
        public virtual DbSet<Menu> Menu { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<Page> Page { get; set; }
        public virtual DbSet<Parameter> Parameter { get; set; }
        public virtual DbSet<Post> Post { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductType> ProductType { get; set; }
        public virtual DbSet<SysActivityLog> SysActivityLog { get; set; }
        public virtual DbSet<SysFunction> SysFunction { get; set; }
        public virtual DbSet<SysFunctionGroupFunction> SysFunctionGroupFunction { get; set; }
        public virtual DbSet<SysGroupFunction> SysGroupFunction { get; set; }
        public virtual DbSet<SysMenuGroup> SysMenuGroup { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserFunction> UserFunction { get; set; }
        public virtual DbSet<UserGroupFunction> UserGroupFunction { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-39C3TKC;Database=BLOG;Integrated Security=true;Persist Security Info=False;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.KeyName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Keywords).HasMaxLength(512);

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.OrderBy).HasDefaultValueSql("((1))");

                entity.Property(e => e.SlugUrl).HasMaxLength(512);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(512);

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(512);
            });

            modelBuilder.Entity<ConstantProductType>(entity =>
            {
                entity.Property(e => e.Size).HasMaxLength(512);

                entity.Property(e => e.Type).HasMaxLength(512);
            });

            modelBuilder.Entity<ImagesProduct>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(512);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(512);

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Menu>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Alias).HasMaxLength(512);

                entity.Property(e => e.MenuIcon).HasMaxLength(50);

                entity.Property(e => e.MenuType).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.OrderBy).ValueGeneratedOnAdd();

                entity.Property(e => e.Target).HasMaxLength(50);

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.Url).HasMaxLength(512);
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(512);

                entity.Property(e => e.FirstName).HasMaxLength(512);

                entity.Property(e => e.LastName).HasMaxLength(512);

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.ProductIds).HasMaxLength(512);

                entity.Property(e => e.ProductTypeIds).HasMaxLength(512);

                entity.Property(e => e.Type).HasMaxLength(512);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Page>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Heading).HasMaxLength(200);

                entity.Property(e => e.Keywords).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Title).HasMaxLength(200);
            });

            modelBuilder.Entity<Parameter>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Key).HasMaxLength(150);

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ViewColumns).HasMaxLength(512);
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(512);

                entity.Property(e => e.FaceIds)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Image).HasMaxLength(512);

                entity.Property(e => e.Keywords).HasMaxLength(512);

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.SlugUrl).HasMaxLength(512);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(512);

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(512);

                entity.HasOne(d => d.Cate)
                    .WithMany(p => p.Post)
                    .HasForeignKey(d => d.CateId)
                    .HasConstraintName("FK_Post_Category");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.CateIds).HasMaxLength(512);

                entity.Property(e => e.CoverImage).HasMaxLength(512);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Keywords).HasMaxLength(512);

                entity.Property(e => e.LongDescription).HasMaxLength(512);

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.ShortDescription).HasMaxLength(512);

                entity.Property(e => e.SlugUrl).HasMaxLength(512);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(512);

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(512);
            });

            modelBuilder.Entity<ProductType>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Size).HasMaxLength(512);

                entity.Property(e => e.Type).HasMaxLength(512);
            });

            modelBuilder.Entity<SysActivityLog>(entity =>
            {
                entity.ToTable("sysActivityLog");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.LogType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ResourceType).HasMaxLength(50);

                entity.Property(e => e.SessionId)
                    .IsRequired()
                    .HasMaxLength(76);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SysActivityLog)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_sysActivityLog_User");
            });

            modelBuilder.Entity<SysFunction>(entity =>
            {
                entity.ToTable("sysFunction");

                entity.Property(e => e.ActionName).HasMaxLength(150);

                entity.Property(e => e.ControllerName).HasMaxLength(150);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Icon).HasMaxLength(50);

                entity.Property(e => e.Key).HasMaxLength(150);

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.MenuGroup)
                    .WithMany(p => p.SysFunction)
                    .HasForeignKey(d => d.MenuGroupId)
                    .HasConstraintName("FK_sysFunction_sysMenuGroup");
            });

            modelBuilder.Entity<SysFunctionGroupFunction>(entity =>
            {
                entity.ToTable("sysFunctionGroupFunction");
            });

            modelBuilder.Entity<SysGroupFunction>(entity =>
            {
                entity.ToTable("sysGroupFunction");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<SysMenuGroup>(entity =>
            {
                entity.ToTable("sysMenuGroup");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Icon).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Tag>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(250);

                entity.Property(e => e.FirstName).HasMaxLength(150);

                entity.Property(e => e.LastName).HasMaxLength(150);

                entity.Property(e => e.Password).HasMaxLength(250);

                entity.Property(e => e.SaltKey).HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserName).HasMaxLength(150);
            });

            modelBuilder.Entity<UserFunction>(entity =>
            {
                entity.Property(e => e.FunctionCode).HasMaxLength(150);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserFunction)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserFunction_User");
            });

            modelBuilder.Entity<UserGroupFunction>(entity =>
            {
                entity.HasOne(d => d.GroupFunction)
                    .WithMany(p => p.UserGroupFunction)
                    .HasForeignKey(d => d.GroupFunctionId)
                    .HasConstraintName("FK_UserGroupFunction_sysGroupFunction");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserGroupFunction)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserGroupFunction_User");
            });
        }
    }
}
