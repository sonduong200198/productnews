﻿using System;
using System.Collections.Generic;

namespace BLOG.Models.Entities
{
    public partial class Post
    {
        public int Id { get; set; }
        public string SlugUrl { get; set; }
        public string Name { get; set; }
        public int? CateId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public string Contents { get; set; }
        public string Status { get; set; }
        public int? CountView { get; set; }
        public string Type { get; set; }
        public string Tags { get; set; }
        public string FaceIds { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public Category Cate { get; set; }
    }
}
