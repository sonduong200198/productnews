﻿using System;
using System.Collections.Generic;

namespace BLOG.Models.Entities
{
    public partial class User
    {
        public User()
        {
            SysActivityLog = new HashSet<SysActivityLog>();
            UserFunction = new HashSet<UserFunction>();
            UserGroupFunction = new HashSet<UserGroupFunction>();
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string SaltKey { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public ICollection<SysActivityLog> SysActivityLog { get; set; }
        public ICollection<UserFunction> UserFunction { get; set; }
        public ICollection<UserGroupFunction> UserGroupFunction { get; set; }
    }
}
