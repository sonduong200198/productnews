﻿using System;
using System.Collections.Generic;

namespace BLOG.Models.Entities
{
    public partial class Menu
    {
        public int Id { get; set; }
        public int? ItemId { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Url { get; set; }
        public string Target { get; set; }
        public string Type { get; set; }
        public string MenuType { get; set; }
        public string MenuIcon { get; set; }
        public int OrderBy { get; set; }
    }
}
