﻿using System;
using System.Collections.Generic;

namespace BLOG.Models.Entities
{
    public partial class ConstantProductType
    {
        public int Id { get; set; }
        public int? Promotion { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
    }
}
