﻿using System;
using System.Collections.Generic;

namespace BLOG.Models.Entities
{
    public partial class SysFunctionGroupFunction
    {
        public int Id { get; set; }
        public int FunctionId { get; set; }
        public int GroupFunctionId { get; set; }
    }
}
