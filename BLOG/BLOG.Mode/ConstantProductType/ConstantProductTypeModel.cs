﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Models.ConstantProductType
{
    public class ConstantProductTypeModel
    {
        public int Id { get; set; }
        public int? Promotion { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
    }
}
