﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Models.ConstantProductType
{
    public class ConstantProductTypeViewModel
    {
        public ConstantProductTypeViewModel()
        {
            InfoConstantProductType =new ConstantProductTypeModel();
            ListConstantProductTypes = new List<ConstantProductTypeModel>();
        }
        public ConstantProductTypeModel InfoConstantProductType { get; set; }
        public List<ConstantProductTypeModel> ListConstantProductTypes { get; set; }
    }
}
