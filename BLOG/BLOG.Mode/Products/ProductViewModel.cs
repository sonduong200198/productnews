﻿using BLOG.Models.ProductType;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Models.Products
{
    public class ProductViewModel : BaseViewModel
    {
        public ProductViewModel()
        {
            InfoProduct = new ProductModel();
            ListProducts = new List<ProductModel>();
            ListTags = new List<SelectListItem>();
            ListCategories= new List<SelectListItem>();
            ListProductTypeTable = new List<ProductTypeModel>();
            ListSizeProduct = new List<SelectListItem>();
            ListTypeProduct=new List<SelectListItem>();
        }
        public ProductModel InfoProduct { get; set; }
        public List<SelectListItem> ListTags { get; set; }
        public List<ProductModel> ListProducts { get; set; }
        public List<SelectListItem> ListCategories { get; set; }
        public List<ProductTypeModel> ListProductTypeTable { get; set; }
        public List<SelectListItem> ListSizeProduct { get; set; }
        public List<SelectListItem> ListTypeProduct { get; set; }

    }
}
