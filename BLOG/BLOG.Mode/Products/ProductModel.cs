﻿using System.Collections.Generic;

namespace BLOG.Models.Products
{
    public class ProductModel : BaseModel
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string CoverImage { get; set; }
        public string Type { get; set; }
        public string SlugUrl { get; set; }
        public string Url { get; set; }
        public string Contents { get; set; }
        public int? NumberProduct { get; set; }
        public int? CountSold { get; set; }
        public int? Count { get; set; }
        public int? Price { get; set; }
        public int? Promotion { get; set; }
        public List<int?> CateProductId { get; set; }
        public string Keywords { get; set; }
        public string Status { get; set; }
        public string Tags { get; set; }
        public string[] ListTagChoosed { get; set; }
        
    }
}
