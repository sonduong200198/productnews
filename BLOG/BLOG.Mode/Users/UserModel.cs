﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BLOG.Models.Sys;

namespace BLOG.Models.Users
{
    public class UserModel:BaseModel
    {
        public UserModel()
        {
            GroupFunctionItems = new List<SysGroupFunctionModel>();
        }

        [Required]
        public string UserName { get; set; }
        public string SaltKey { get; set; }

        [Required]
        public string Password { get; set; }
        public string Email { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        public int? Status { get; set; }

        //Extend
        public string FullName => $"{FirstName} {LastName}";

        public List<SysGroupFunctionModel> GroupFunctionItems { get; set; }
    }
}
