﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Models.Parameter
{
    public class ParameterViewModel :BaseViewModel
    {
        public ParameterViewModel()
        {
            InfoParameter = new ParameterModel();
            ListParameters = new List<ParameterModel>();
        }
        public ParameterModel InfoParameter { get; set; }
        public List<ParameterModel> ListParameters { get; set; }
    }
}
