﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace BLOG.Core.Extensions
{
    public static class HtmlExtensions
    {
        public static string IsSelected(this IHtmlHelper html, string controllers = null, string action = null, string cssClass = null)
        {

            if (string.IsNullOrEmpty(cssClass))
                cssClass = "active";

            var currentAction = (string)html.ViewContext.RouteData.Values["action"];
            var currentController = (string)html.ViewContext.RouteData.Values["controller"];
            //var status = html.ViewContext.HttpContext.Request.Query["type"].ToString();
            var parameter = (string)html.ViewContext.RouteData.Values["type"];
            if (string.IsNullOrEmpty(controllers))
                controllers = currentController;
            if (parameter != null)
            {
                currentAction = (currentAction + "/" + parameter);
            }
            if (string.IsNullOrEmpty(action))
                action = currentAction;

            currentAction = currentAction.ToLower();
            action = action.ToLower();
            currentController = currentController.ToLower();
            controllers = controllers.ToLower();

            var list = controllers.Split(new[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
            if (list.Contains(currentController) && action == currentAction)
            {
                return cssClass;
            }
            return string.Empty;
        }

        public static string GetActive(this IHtmlHelper html, string path)
        {
            var uri = html.ViewContext.HttpContext.Request.Path.Value + html.ViewContext.HttpContext.Request.QueryString.Value;
            if (uri.Contains(path)) return "active";
            return string.Empty;
        }
    }
}
