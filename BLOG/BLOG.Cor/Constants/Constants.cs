﻿using System.ComponentModel;

namespace BLOG.Core.Constants
{
    public static class Constants
    {
        public const string AdminUserName = "Admin";
        public const string SessionMenu = "SessionMenu";
        public class ConfigKey
        {
            public const string EmailAccount = "EmailAccount";
        }

        public class SortByData
        {
            public const string Name = "NAME";

            public const string Status = "STATUS";

            public const string CreateDate = "CREATEDATE";

            public const string UpdatedDate = "UPDATEDDATE";

            public const string OrderBy = "ORDERBY";
        }

        public class OrderByData
        {
            public const string Desc = "DESC";

            public const string Asc = "ASC";
        }
        public class AdminstratorUrl
        {
            public const string adminstrator = "adminstrator";
        }
        public class Status
        {
            [Description("Publish")]
            public const string Publish = "PUBLISH";

            [Description("Draft")]
            public const string Draft = "DRAFT";

            [Description("Delete")]
            public const string Delete = "DELETE";
            [Description("Unavailable")]
            public const string Unavailable = "Unavailable";
            [Description("Avalible")]
            public const string Available = "Available";
            [Description("Active")]
            public const int Active = 1;

            [Description("In Active")]
            public const int InActive = 0;

        }

        public class PostType
        {

            public const string Bank = "BANK";

            public const string Blog = "BLOG";

            public const string New = "NEW";

        }
        public class CategoryType
        {      
            public const string Blog = "BLogs";
            public const string General = "General";
            public const string Pro = "Product";
        }

        public class StatusJsonResponse
        {
            public const string Success = "OK";
            public const string Error = "NG";
        }
        public class NameForderUploadImage
        {
            public const string ForderImage = "images";
        }
        public class Message
        {
            public const string DeleteConfirm = "Are you sure you want to delete {0}?";
            public const string IsExists = "{0} is existed";
            public const string IsNotExists = "{0} is not existed";
            // Create
            public const string FailToCreate = "This {0} has been create unsuccessfully";
            public const string SuccessToCreate = "This {0} has been create successfully";

            // Update
            public const string FailToUpdate = "This {0} has been update unsuccessfully";
            public const string SuccessToUpdate = "This {0} has been update successfully";
            public const string ChangeStatus = "This {0} has changed to {1}";

            // Delete
            public const string FailToDelete = "This {0} has been delete unsuccessfully";
            public const string SuccessToDelete = "This {0} has been delete successfully";

            //Sendmail
            public const string SendEmailSuccess = "Email sent successfully";
            public const string SendEmailError = "Email sent failed";

            //Validate
            public const string ValidateFormat = "Wrong {0} format";
            public const string ValidateFail = "Please enter the correct and complete information";

            public const string PasswordIncorrect = "Current password is incorrect";

            //Error
            public const string ApplicationError = "This action can not be executed by the cause of the network or server. Please try again a few times!";

            //upload image
            public const string UploadImage = "Image password is incorrect";
        }

        public class TempDataKey
        {
            public const string Success = "SUCCESS";
            public const string Error = "ERROR";
        }

        public class LogType
        {
            public const string Insert = "INSERT";
            public const string Update = "UPDATE";
            public const string Delete = "DELETE";
            public const string Search = "SEARCH";
            public const string SendEmail = "SENDEMAIL";
        }

        public class ResourceType
        {
            public const string Admin = "ADMIN";
            public const string Api = "API";
        }

        public class MessageLog
        {
            public const string Insert = "Insert {0}: {1}";
            public const string Update = "Update {0}: {1}";
            public const string Delete = "Delete {0}: {1}";
            public const string SendEmail = "Send email {0}: {1}";
        }
    }
}
