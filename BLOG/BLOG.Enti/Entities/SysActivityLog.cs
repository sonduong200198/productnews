﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities.Entities
{
    public partial class SysActivityLog
    {
        public int Id { get; set; }
        public string LogType { get; set; }
        public string SessionId { get; set; }
        public string Comment { get; set; }
        public string ResourceType { get; set; }
        public int? UserId { get; set; }
        public DateTime CreateDate { get; set; }

        public User User { get; set; }
    }
}
