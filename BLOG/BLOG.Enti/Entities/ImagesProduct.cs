﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities.Entities
{
    public partial class ImagesProduct
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public int ProductId { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
