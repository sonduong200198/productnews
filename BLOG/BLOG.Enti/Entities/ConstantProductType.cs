﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities.Entities
{
    public partial class ConstantProductType
    {
        public int Id { get; set; }
        public int? Promotion { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
    }
}
