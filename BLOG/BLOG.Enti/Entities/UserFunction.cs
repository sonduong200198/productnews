﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities.Entities
{
    public partial class UserFunction
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string FunctionCode { get; set; }

        public User User { get; set; }
    }
}
