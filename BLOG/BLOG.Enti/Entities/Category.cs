﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities.Entities
{
    public partial class Category
    {
        public Category()
        {
            Post = new HashSet<Post>();
        }

        public int Id { get; set; }
        public string KeyName { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string SlugUrl { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }
        public int? OrderBy { get; set; }
        public string Status { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public ICollection<Post> Post { get; set; }
    }
}
