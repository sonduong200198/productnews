﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities.Entities
{
    public partial class Order
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Type { get; set; }
        public string Phone { get; set; }
        public string ProductTypeIds { get; set; }
        public string ProductIds { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Note { get; set; }
    }
}
