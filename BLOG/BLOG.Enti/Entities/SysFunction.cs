﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities.Entities
{
    public partial class SysFunction
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public int? OrderBy { get; set; }
        public bool? ShowOnMenu { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public string Icon { get; set; }
        public int? MenuGroupId { get; set; }

        public SysMenuGroup MenuGroup { get; set; }
    }
}
