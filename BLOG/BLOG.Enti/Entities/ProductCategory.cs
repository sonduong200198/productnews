﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities.Entities
{
    public partial class ProductCategory
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int? CategoryId { get; set; }
    }
}
