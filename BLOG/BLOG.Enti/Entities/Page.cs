﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities.Entities
{
    public partial class Page
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Heading { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public int? CreateBy { get; set; }
        public int? UpdateBy { get; set; }
    }
}
