﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities.Entities
{
    public partial class SysFunctionGroupFunction
    {
        public int Id { get; set; }
        public int FunctionId { get; set; }
        public int GroupFunctionId { get; set; }
    }
}
