﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using BLOG.Business.Sys;
using BLOG.Business.Users;
using BLOG.Core.Constants;
using BLOG.Core.Utility;
using BLOG.Models.Sys;

namespace BLOG.App_Core.Attributes
{
    public class UserAuthorizeAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!context.HttpContext.User.Identity.IsAuthenticated)
            {
                var loginRoute = new RouteValueDictionary(new { controller = "Login", action = "Index", returnUrl = $"{context.HttpContext.Request.Path.Value}{context.HttpContext.Request.QueryString.Value}" });
                context.Result = new RedirectToRouteResult(loginRoute);
                return;
            }
            var userService = (IUserService)context.HttpContext.RequestServices.GetService(typeof(IUserService));
            var menuGroupService = (ISysMenuGroupService)context.HttpContext.RequestServices.GetService(typeof(ISysMenuGroupService));
            var userName = context.HttpContext.User.Identity.Name;
            
            if (Constants.AdminUserName.Equals(userName, StringComparison.OrdinalIgnoreCase))
            {
                var functionService = (ISysFunctionsService)context.HttpContext.RequestServices.GetService(typeof(ISysFunctionsService));
                var functionAll = functionService.GetAll().ToList();
                var menuAllItems = menuGroupService.GetMenuGroupAll();
                foreach (var menu in menuAllItems)
                {
                    menu.MenuItems = functionAll.Where(m => m.MenuGroupId == menu.Id).Select(c => new MenuItemModel
                    {
                        Id = c.Id,
                        Icon = c.Icon,
                        Name = c.Name,
                        ControllerName = c.ControllerName,
                        ActionName = c.ActionName,
                        OrderBy = c.OrderBy ?? 1
                    }).OrderBy(m => m.OrderBy).ToList();
                }
                Extensions.SessionExtensions.Set<MenuPermissionModel>(context.HttpContext.Session, Constants.SessionMenu, menuAllItems);
                return;
            }
            var controllerName = context.ActionDescriptor.RouteValues["controller"].ToLower();
            var actionName = context.ActionDescriptor.RouteValues["action"].ToLower();
            
            var accessDeny = new RouteValueDictionary(
                new
                {
                    Areas= "Administrator",
                    controller = "Errors",
                    action = "AccessDenied",
                    returnUrl = $"{context.HttpContext.Request.Path.Value}{context.HttpContext.Request.QueryString.Value}"
                });

            var functionAllowed = userService.GetPermissionsByUser(userName);
            var controllerAllowed = functionAllowed.Select(c=>c.ControllerName.ToLower()).ToList();
            controllerAllowed.Add("home");
            List<string> urlNotPermissions = new List<string>() { "users/changepassword" };
            if (!controllerAllowed.Contains(controllerName) && !urlNotPermissions.Contains($"{controllerName.ToLower()}/{actionName.ToLower()}"))
            {
                context.Result = new RedirectToRouteResult(accessDeny);
                return;
            }

            var menuItems = menuGroupService.GetMenuGroupByIds(functionAllowed.Select(c => NumberTools.ConvertToInt(c.MenuGroupId)).ToList());
            foreach (var menu in menuItems)
            {
                menu.MenuItems = functionAllowed.Where(m => m.MenuGroupId == menu.Id).Select(c => new MenuItemModel
                {
                    Id = c.Id,
                    Icon = c.Icon,
                    Name = c.Name,
                    ControllerName = c.ControllerName,
                    ActionName = c.ActionName,
                    OrderBy = c.OrderBy??1
                }).OrderBy(m => m.OrderBy).ToList();
            }
            Extensions.SessionExtensions.Set<MenuPermissionModel>(context.HttpContext.Session, Constants.SessionMenu, menuItems);
        }
    }
}
