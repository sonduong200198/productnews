﻿using AutoMapper;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Parameter;

namespace BLOG.App_Core.Infrastructure
{
    public class ParameterMappingProfile :Profile
    {
        public ParameterMappingProfile()
        {
            CreateMap<Parameter, ParameterModel>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.CreatedDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.UpdatedDate)));
            CreateMap<ParameterModel, Parameter>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.CreatedDate)))
            .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.UpdatedDate)));
        }
    }
}
