﻿using AutoMapper;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Order;

namespace BLOG.App_Core.Infrastructure
{
    public class OrderMappingProfile : Profile
    {
        // Category
        public OrderMappingProfile()
        {
            CreateMap<Order, OrderModel>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.CreateDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.UpdateDate)));
            CreateMap<OrderModel, Order>()
                .ForMember(dest => dest.CreateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.CreatedDate)))
            .ForMember(dest => dest.UpdateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.UpdatedDate)));


        }
    }
}
