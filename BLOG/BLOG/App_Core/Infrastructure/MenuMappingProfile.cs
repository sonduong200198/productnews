﻿using AutoMapper;
using BLOG.Entities.Entities;
using BLOG.Models.Menu;

namespace BLOG.App_Core.Infrastructure
{
    public class MenuMappingProfile : Profile
    {
        // Menu
        public MenuMappingProfile()
        {
            CreateMap<Menu, MenuModel>();
            CreateMap<MenuModel, Order>();
        }
    }
}
