﻿using AutoMapper;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Users;

namespace BLOG.App_Core.Infrastructure
{
    internal class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            // User
            CreateMap<User, UserModel>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.CreatedDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.UpdatedDate)));
            CreateMap<UserModel, User>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => DateTimeTools.ConvertToDatetime(src.CreatedDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.UpdatedDate)));

        }
    }
}
