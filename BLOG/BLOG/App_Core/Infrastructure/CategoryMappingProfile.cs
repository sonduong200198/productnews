﻿using AutoMapper;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Categories;

namespace BLOG.App_Core.Infrastructure
{
    internal class CategoryMappingProfile : Profile
    {
        // Category
        public CategoryMappingProfile()
        {
            CreateMap<Category, CategoryModel>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.CreateDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.UpdateDate)));
            CreateMap<CategoryModel, Category>()
                .ForMember(dest => dest.CreateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.CreatedDate)))
            .ForMember(dest => dest.UpdateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.UpdatedDate)));


        }
    }
}
