﻿using AutoMapper;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Posts;

namespace BLOG.App_Core.Infrastructure
{
    public class PostsMappingProfile :Profile
    {
        public PostsMappingProfile()
        {
            CreateMap<Tag, TagModel>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.CreateDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.UpdateDate)));
            CreateMap<TagModel, Tag>()
                .ForMember(dest => dest.CreateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.CreatedDate)))
            .ForMember(dest => dest.UpdateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.UpdatedDate)));

            CreateMap<Post, PostModel>().ForMember(dest => dest.Link, opt => opt.MapFrom(src => string.Format("/blogs/{0}-{1}", src.SlugUrl, src.Id)))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.CreateDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.UpdateDate)));
            CreateMap<PostModel, Post>()
                .ForMember(dest => dest.CreateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.CreatedDate)))
            .ForMember(dest => dest.UpdateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.UpdatedDate)));

        }
    }
}
