﻿using AutoMapper;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.ImagesProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLOG.App_Core.Infrastructure
{
    public class ImagesProductMappingProfile : Profile
    {
        // Category
        public ImagesProductMappingProfile()
        {
            CreateMap<ImagesProduct, ImageProductModel>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.CreateDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.UpdateDate)));
            CreateMap<ImageProductModel, ImagesProduct>()
                .ForMember(dest => dest.CreateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.CreatedDate)))
            .ForMember(dest => dest.UpdateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.UpdatedDate)));


        }
    }
}
