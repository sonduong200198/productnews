﻿using AutoMapper;
using BLOG.Entities.Entities;
using BLOG.Models.ProductType;

namespace BLOG.App_Core.Infrastructure
{
    public class ProductTypeMappingProfile : Profile
    {
        // Category
        public ProductTypeMappingProfile()
        {
            CreateMap<ProductType, ProductTypeModel>();
            CreateMap<ProductTypeModel, ProductType>();
        }
    }
}
