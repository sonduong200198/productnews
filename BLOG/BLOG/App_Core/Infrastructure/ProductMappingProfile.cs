﻿using AutoMapper;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Products;

namespace BLOG.App_Core.Infrastructure
{
    public class ProductMappingProfile : Profile
    {
        // Category
        public ProductMappingProfile()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.CreateDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.UpdateDate)));
            CreateMap<ProductModel, Product>()
                .ForMember(dest => dest.CreateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.CreatedDate)))
            .ForMember(dest => dest.UpdateDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.UpdatedDate)));


        }
    }
}
