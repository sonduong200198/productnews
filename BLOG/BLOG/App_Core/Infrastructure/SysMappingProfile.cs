﻿using AutoMapper;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Sys;

namespace BLOG.App_Core.Infrastructure
{
    internal class SysMappingProfile : Profile
    {
        public SysMappingProfile()
        {
            // SysActivityLog
            CreateMap<SysActivityLog, SysActivityLogModel>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToFullString(src.CreateDate)));

            // SysMenuGroup
            CreateMap<SysMenuGroup, SysMenuGroupModel>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.CreatedDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.UpdatedDate)));
            CreateMap<SysMenuGroupModel, SysMenuGroup>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => DateTimeTools.ConvertToDatetime(src.CreatedDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.UpdatedDate)));

            // SysGroupFunction
            CreateMap<SysGroupFunction, SysGroupFunctionModel>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.CreatedDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.UpdatedDate)));
            CreateMap<SysGroupFunctionModel, SysGroupFunction>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => DateTimeTools.ConvertToDatetime(src.CreatedDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.UpdatedDate)));

            //SysFunctions
            CreateMap<SysFunction, SysFunctionsModel>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.CreatedDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => StringTools.ConvertToString(src.UpdatedDate)));
            CreateMap<SysFunctionsModel, SysFunction>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => DateTimeTools.ConvertToDatetime(src.CreatedDate)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => DateTimeTools.CFToDateTime(src.UpdatedDate)));
        

        }
    }
}
