﻿using System;
using System.IO;
using AutoMapper;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using BLOG.App_Core.Infrastructure;
using BLOG.Business.Sys;
using BLOG.Business.Users;
using BLOG.Entities.Entities;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using BLOG.Entities;
using BLOG.Business.Posts;
using BLOG.Business.Categories;
using BLOG.Business.Parameters;
using BLOG.Business.Order;
using BLOG.Business.Product;
using BLOG.Business.ProductType;
using BLOG.Business.ImagesProduct;
using BLOG.Business.Menu;
using BLOG.Busin.ConstantProductType;
using BLOG.Busin.ProductCategory;

namespace BLOG
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            this.Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
            {
                options.LoginPath = "/Login";
                options.AccessDeniedPath = "/Errors/AccessDenied";
                options.Cookie.Name = ".BLOG.Authenticate";
            });

            // Database registers
            services.AddDbContext<BLOG.Entities.Entities.BLOGContext>(options => options.UseSqlServer(Configuration.GetConnectionString("dbConn")));

            services.AddSingleton<IFileProvider>(new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));

            //Start Registering and Initializing AutoMapper
            var cfg = new MapperConfigurationExpression();
            cfg.AddProfile<SysMappingProfile>();
            cfg.AddProfile<UserMappingProfile>();
            cfg.AddProfile<MenuMappingProfile>();
            cfg.AddProfile<OrderMappingProfile>();
            cfg.AddProfile<CategoryMappingProfile>();
            cfg.AddProfile<PostsMappingProfile>();
            cfg.AddProfile<ParameterMappingProfile>();
            cfg.AddProfile<ImagesProductMappingProfile>();
            cfg.AddProfile<ProductMappingProfile>();
            cfg.AddProfile<ProductTypeMappingProfile>();

            Mapper.Initialize(cfg);
            services.AddAutoMapper(typeof(Startup).Assembly);


            // Register Components
            RegisterDependencyInjector(services);

            services.AddMvc().AddRazorPagesOptions(options => options.AllowAreas = true).SetCompatibilityVersion(CompatibilityVersion.Version_2_1); //.AddSessionStateTempDataProvider();

            services.AddSession(options =>
            {
                options.Cookie.Name = ".BLOG.Session";
                options.IdleTimeout = TimeSpan.FromMinutes(30);
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<BLOG.Entities.Entities.BLOGContext>();
                context.Database.Migrate();
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseSession();
            app.UseAuthentication();
            app.UseMvcWithDefaultRoute();
            app.UseMvc(routes =>
            {
                //routes.MapAreaRoute(
                //    name: "Manager",
                //    areaName: "Administrator",
                //    template: "{area:exists}/Manager/Pages/{type?}",
                //    defaults: new { controller = "Manager", action = "Pages" });
                routes.MapAreaRoute(
                    name: "Administrator",
                    areaName: "Administrator",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}/{text?}");
                routes.MapRoute(
                    name: "Banks",
                    template: "{controller=Home}/{action=Banks}/{id?}");
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseCookiePolicy();
        }

        void RegisterDependencyInjector(IServiceCollection services)
        {
            #region #Sys

            services.AddScoped<ISysActivityLogService, SysActivityLogService>();
            services.AddScoped<ISysFunctionGroupFunctionService, SysFunctionGroupFunctionService>();
            services.AddScoped<ISysFunctionsService, SysFunctionsService>();
            services.AddScoped<ISysGroupFunctionService, SysGroupFunctionService>();
            services.AddScoped<ISysMenuGroupService, SysMenuGroupService>();

            #endregion

            #region #User

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserFunctionService, UserFunctionService>();
            services.AddScoped<IUserGroupFunctionsService, UserGroupFunctionsService>();

            #endregion


            #region #ProductCategory
            services.AddScoped<IProductCategoryService,ProductCategoryService>();
            #endregion
            #region #Post
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<ITagService, TagService>();
            #endregion
            #region #Parameter
            services.AddScoped<IParameterService, ParameterService>();
            #endregion
            #region #Category
            services.AddScoped<ICategoryService, CategoryService>();
            #endregion
            #region #Order
            services.AddScoped<IOrderService, OrderService>();
            #endregion
            #region #Product
            services.AddScoped<IProductService, ProductService>();
            #endregion
            #region #ProductType
            services.AddScoped<IProductTypeService, ProductTypeService>();
            #endregion
            #region #ImagesProduct
            services.AddScoped<IImagesProductService, ImagesProductService>();
            #endregion
            #region #Menu
            services.AddScoped<IMenuService, MenuService>();
            #endregion
            #region #ConstantProductType
            services.AddScoped<IConstantProductTypeService, ConstantProductTypeService>();
            #endregion
        }
    }
}
