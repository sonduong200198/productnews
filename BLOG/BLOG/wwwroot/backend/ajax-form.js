﻿function deleteItem(url) {
    window.deleteUrl = url;
    var placeholderElement = $('#modal-placeholder');
    var modal1 = $('#modal-placeholder .modal.show');
    if (modal1.length) placeholderElement = $('#modal-placeholder2');

    var text = document.getElementById('exampleModal').outerHTML;
    placeholderElement.html(text);
    placeholderElement.find('.modal').modal('show');
}
function requestDelete() {
    $.get(window.deleteUrl).done(function (data) {
        reloadPage();
        showSuccessMessage("Successful implementation!");
    });
}
function keypressSearch() {
    if (event.keyCode == 13) searchItems();
}
function searchItems() {
    var url = window.location.href;
    var index = url.indexOf('keyword=');
    if (index > 0) url = url.substr(0, index - 1);
    if (url.indexOf('?') == -1) {
        url += '?';
    }
    else {
        url += '&';
    }

    url += 'keyword=' + $('[name="keyword"]').val().trim();
    url = url.replace(/ /g, '-');
    $('[data-url-refresh]').attr('data-url-refresh', url);
    reloadPage();
}
$(".js-example-tags").select2({
    tags: true
});

//UrlSlug

function openModalForm() {
    var data = $(event.target).data();
    var dataURL = data.popupUrl; //"/career/ApplyForm?id=" + data.id;
    $('#requestModal .modal-dialog').load(dataURL, function () {
        $('#requestModal').modal({ show: true });
        $.validator.unobtrusive.parse("#request_form");
    });
}

function openPopupForm(link) {
    var modal1 = $('#modal-placeholder .modal.show');
    var url = $(event.target).data('url');
    if (link) url = link;

    if (modal1.length) {
        $.get(url).done(function (data) {
            var placeholderElement = $('#modal-placeholder2');
            placeholderElement.html(data);
            placeholderElement.find('.modal').modal('show');
        });
        return;
    }
    $.get(url).done(function (data) {
        var placeholderElement = $('#modal-placeholder');
        placeholderElement.html(data);
        placeholderElement.find('#modal-id').modal('show');
        $("#exampleModal").modal('hide');

    });
}

function savePopupForm() {
    //validate year
    var year = $("#year-val").val();
    if (year == "") {
        $("#message-year").text("The Year is required");
        $("#create-year").attr("disabled", true);
        return;
    }
    //test send file
    $("#bankId").val($("#idbank").val());
    var form = $(event.target).parents('.modal').find('form')[0];
    var actionUrl = $(event.target).parents('.modal').find('form').attr('action');

    var data = new FormData(form);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: actionUrl,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
            var newBody = $('.modal-body', data);
            var placeholderElement = $('#modal-placeholder');

            var modal2 = $('#modal-placeholder2 .modal.show');
            if (modal2.length) placeholderElement = $('#modal-placeholder2');

            placeholderElement.find('.modal-body').replaceWith(newBody);

            // find IsValid input field and check it's value
            // if it's valid then hide modal window
            var isValid = newBody.find('[name="IsValid"]').val() == 'True';
            if (isValid) {
                placeholderElement.find('.modal').modal('hide');
                placeholderElement.find('.modal').empty();
                $("#exampleModal").modal('hide');
                showSuccessMessage("Successful implementation!");
                reloadPage();
            }
            
        }
    });
}
function reloadPage() {
    var refreshArea = $('[data-url-refresh]').last();
    var url = refreshArea.attr('data-url-refresh');
    $.get(url).done(function (data) {
        var refreshedData = $(data).find('[data-url-refresh]').html();
        refreshArea.html(refreshedData);
    });    
}

//link to function delete in controller
//var url = "";
//function deleteJs(link) {
//    $('[data-url-refresh]').attr('data-url-refresh', window.location.href);
//    $('#exampleModal').modal('show');
//    if (link) url = link;
//}

//function confirmDelete() {
//    $.get(url).done(function (data) {
//        $('#exampleModal').modal('hide');
//        reloadPage();
//    });
//}
function pagingNotReload(pageIndex) {
    var url = window.location.href;
    if (url.indexOf("?pageIndex") != -1) {
        urlSplit = url.split("?pageIndex")[0];
        url = urlSplit + "?pageIndex=" + pageIndex;
    }
    if (url.indexOf("&pageIndex") != -1) {
        urlSplit = url.split("&pageIndex")[0];
        url = urlSplit + "&pageIndex=" + pageIndex;
    }

    if (url.indexOf("?pageIndex") == -1 && url.indexOf("&pageIndex") == -1 && url.indexOf("?") == -1) {
        url += "?pageIndex=" + pageIndex;
    }
    if (url.indexOf("?pageIndex") == -1 && url.indexOf("&pageIndex") == -1 && url.indexOf("?") != -1) {
        url += "&pageIndex=" + pageIndex;
    }
    $('[data-url-refresh]').attr('data-url-refresh', url);
    reloadPage();
    window.history.pushState('page2', 'Title', url);
}
function searchKeypress() {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        var valueInput = $(".search-input").val();
        var url = window.location.href;
        if (url.indexOf("?pageIndex") != -1) {
            url = url.split("?pageIndex")[0];
        }
        if (url.indexOf("&pageIndex") != -1) {
            url = url.split("&pageIndex")[0];
        }
        if (url.indexOf("keyword") == -1) {
            if (url.indexOf("?") == -1) {
                url += "?keyword=";
            }
            else {
                url += "&keyword=";
            }
            url += valueInput;

        } else {
            var urlSplit = url.split("keyword")[0];
            url = urlSplit + "keyword=" + valueInput;
        }
        $('[data-url-refresh]').attr('data-url-refresh', url);
        reloadPage();
        window.history.pushState('page2', 'Title', url);
    }
}
function searchClick() {
    var valueInput = $(".search-input").val();
    var url = window.location.href;
    if (url.indexOf("?pageIndex") != -1) {
        url = url.split("?pageIndex")[0];
    }
    if (url.indexOf("&pageIndex") != -1) {
        url = url.split("&pageIndex")[0];
    }
    if (url.indexOf("keyword") == -1) {
        if (url.indexOf("?") == -1) {
            url += "?keyword=";
        }
        else {
            url += "&keyword=";
        }
        url += valueInput;

    } else {
        var urlSplit = url.split("keyword")[0];
        url = urlSplit + "keyword=" + valueInput;
    }
    $('[data-url-refresh]').attr('data-url-refresh', url);
    reloadPage();
    window.history.pushState('page2', 'Title', url);
}
$("#frmAdd").validate({
    rules: {
        phone: {
            required: true,
        },
        totalAsset: {
            number: true
        },
        totalProfit: {
            number: true
        },
        totalProfit: {
            number: true
        },
        equityOwner: {
            number: true
        }
    }
});
$(document).ready(function () {
//    $("[for-slug]").blur(function () {
//        var name = $(event.target).attr('for-slug');
//        var text = $(event.target).val();
//        text = text.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/đ/g, "d").replace(/'/g, "").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i").replace(/[^\w\s]/gi, ' ').replace(/  +/g, '-');
//        var text = text.split(" ").join("-").toLowerCase();
//        $('[Name="' + name + '"]').val(text);
//    })
//})
$('body').delegate('input', 'blur', function () {
    $("[for-slug]").blur(function () {
        var name = $(event.target).attr('for-slug');
        var text = $(event.target).val();
        text = text.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/đ/g, "d").replace(/'/g, "").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i").replace(/[^\w\s]/gi, ' ').replace(/  +/g, '-');
        var text = text.split(" ").join("-").toLowerCase();
        $('[Name="' + name + '"]').val(text);
    })
})
});
//function openModalForm() {
//    var data = $(event.target).data();
//    var dataURL = data.popupUrl; //"/career/ApplyForm?id=" + data.id;
//    $('#requestModal .modal-dialog').load(dataURL, function () {
//        $('#requestModal').modal({ show: true });
//        $.validator.unobtrusive.parse("#request_form");
//    });
//}

//function openPopupForm(link) {
//    var url = $(event.target).data('url');
//    if (link) url = link;

//    var placeholderElement = $('#modal-placeholder');
//    var modal1 = $('#modal-placeholder .modal.show');
//    if (modal1.length) placeholderElement = $('#modal-placeholder2');

//    $.get(url).done(function (data) {
//        placeholderElement.html(data);
//        placeholderElement.find('.modal').modal('show');
//    });
//}

//function savePopupForm() {
//    //var form = $(event.target).parents('.modal').find('form');
//    //var actionUrl = form.attr('action');
//    //var dataToSend = form.serialize();
//    //debugger;
//    //$.post(actionUrl, dataToSend).done(function (data) {
//    //    var newBody = $('.modal-body', data);
//    //    var placeholderElement = $('#modal-placeholder');
//    //    placeholderElement.find('.modal-body').replaceWith(newBody);

//    //    // find IsValid input field and check it's value
//    //    // if it's valid then hide modal window
//    //    var isValid = newBody.find('[name="IsValid"]').val() == 'True';
//    //    if (isValid) {
//    //        placeholderElement.find('.modal').modal('hide');
//    //        location.reload();
//    //    }
//    //});

//    //validate year
//    var year = $("#year-val").val();
//    //var image = $("#img").attr('src');
//    //var fileImage = $("#image-val").val();
//    if (year == "") {
//        $("#message-year").text("The Year is required");
//        $("#create-year").attr("disabled", true);
//        return;
//    }
//    //if (fileImage == null && image == "" || image == null) {
//    //    $("#message-image").text("The image is required");
//    //    $("#create-year").attr("disabled", true);
//    //    return;
//    //} else {
//    //    $("#create-year").attr("disabled", false);
//    //}
 
//    //test send file
//    $("#bankId").val($("#idbank").val());
//    var form = $(event.target).parents('.modal').find('form')[0];
//    var actionUrl = $(event.target).parents('.modal').find('form').attr('action');

//    var data = new FormData(form);
//    $.ajax({
//        type: "POST",
//        enctype: 'multipart/form-data',
//        url: actionUrl,
//        data: data,
//        processData: false,
//        contentType: false,
//        cache: false,
//        timeout: 600000,
//        success: function (data) {
//            var newBody = $('.modal-body', data);
//            var placeholderElement = $('#modal-placeholder');

//            var modal2 = $('#modal-placeholder2 .modal.show');
//            if (modal2.length) placeholderElement = $('#modal-placeholder2');

//            placeholderElement.find('.modal-body').replaceWith(newBody);

//            // find IsValid input field and check it's value
//            // if it's valid then hide modal window
//            var isValid = newBody.find('[name="IsValid"]').val() == 'True';
//            if (isValid) {
//                placeholderElement.find('.modal').modal('hide');
//                placeholderElement.find('.modal').empty();
//                reloadPage();
//            }
//        }
//    });
//}

//function reloadPage() {
//    var refreshArea = $('[data-url-refresh]').last();
//    var refreshUrl = refreshArea.attr('data-url-refresh');
//    $.get(refreshUrl).done(function (data) {
//        var refreshedData = $(data).find('[data-url-refresh]').html();
//        //var refreshUrl = 
//        //refreshedData.attr('data-url-refresh', $(data).find('[data-url-refresh]').data('urlRefresh'));
//        refreshArea.html(refreshedData);
//    });
//}
$(document).ready(function () {
    console.log(sum(10))
})

function sum(n) {
    debugger;
    if (n >= 1) {
        return sum(n - 1) + n;
    }
    return n;
}

