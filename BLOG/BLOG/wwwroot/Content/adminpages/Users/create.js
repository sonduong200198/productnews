﻿var validateUser = function (name, id) {
    var validated = true;

    if (name !== undefined && name != null && name !== "") {
        $.ajax({
            async: false,
            type: "POST",
            url: "/Administrator/Users/ExistUserName",
            data: {
                "id": id,
                "name": name
            },
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                // Stop progress
                if (data !== "")
                    validated = false;
            },
            error: function (xhr, status, error) {

            }
        });
    }
    return validated;
};

var viewModel = new function () {
    var self = this;

    self.ItemId = ko.observable(0);

    self.StatusId = ko.observable(data.userInfo.status);
    self.StatusOptions = ko.observableArray(data.statusOptions != null ? data.statusOptions : []);

    self.UserName = ko.observable(data.userInfo.userName).extend({ required: true, whiteSpace: true }).extend({
        validation: {
            validator: function (val) {
                return validateUser(val, 0);
            },
            message: "Username already exists!",
            params: self.UserName
        }
    });
    self.Password = ko.observable('').extend({ required: true, whiteSpace: true, minLength: { params: 6, message: "Please enter at least 6 characters." }, maxLength: { params: 12, message: "Please enter no more than 12 characters." }  });
    self.FirstName = ko.observable(data.userInfo.firstName).extend({ required: true, maxLength: { params: 150 , message: "Please enter no more than 150 characters." }  });
    self.LastName = ko.observable(data.userInfo.lastName).extend({ required: true, maxLength: { params: 150, message: "Please enter no more than 150 characters." } });
    self.Email = ko.observable(data.userInfo.email).extend({ maxLength: { params: 150, message: "Please enter no more than 150 characters." }, email: true });

    self.Submit = function () {
        if (viewModel.errors().length !== 0) {
            viewModel.errors.showAllMessages();
        } else {
            $('#frmAdd').submit();
        }
    }
}

viewModel.errors = ko.validation.group(viewModel);
ko.validation.rules.pattern.message = 'Invalid.';
ko.applyBindings(viewModel, document.getElementById("divList"));

