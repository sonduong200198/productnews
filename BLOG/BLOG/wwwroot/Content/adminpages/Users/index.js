﻿var viewModel = new function () {
    var self = this;

    self.ItemId = ko.observable(0);
    self.StatusId = ko.observable('');
    self.StatusOptions = ko.observableArray(data.statusOptions != null ? data.statusOptions : []);

    self.GroupId = ko.observable('');
    self.GroupOptions = ko.observableArray(data.groupOptions != null ? data.groupOptions : []);

    self.UserItems = ko.observableArray([]);

    // Paginate
    self.Keyword = ko.observable('');   
    self.SortBy = ko.observable('UPDATEDDATE');
    self.OrderBy = ko.observable('DESC');
    self.PageIndex = ko.observable(1);
    self.PageSize = ko.observable(10);
    self.TotalPages = ko.observable(0);
    self.TotalCount = ko.observable(0);
    self.PageSlide = ko.observable(4);

    self.LastPage = ko.computed(function () {
        return Math.floor((self.TotalCount() - 1) / self.PageSize()) + 1;
    });
    self.Pages = ko.computed(function () {
        var pageCount = self.LastPage();
        var pageFrom = Math.max(1, self.PageIndex() - self.PageSlide());
        var pageTo = Math.min(pageCount, self.PageIndex() + self.PageSlide());
        pageFrom = Math.max(1, Math.min(pageTo - 2 * self.PageSlide(), pageFrom));
        pageTo = Math.min(pageCount, Math.max(pageFrom + 2 * self.PageSlide(), pageTo));

        var result = [];
        for (var i = pageFrom; i <= pageTo; i++) {
            result.push({ pageNumber: (i) });
        }
        return result;
    });
    // Prev | Next page
    self.PrevPage = function () {
        var pageIndex = viewModel.PageIndex();
        if (pageIndex > 1) {
            viewModel.PageIndex(pageIndex - 1);
            viewModel.Search();
        }
    };
    self.NextPage = function () {
        var pageIndex = viewModel.PageIndex();
        if (pageIndex < viewModel.TotalPages()) {
            viewModel.PageIndex(pageIndex + 1);
            viewModel.Search();
        }
    };
    self.MoveToPage = function (index) {
        self.PageIndex(index);
        viewModel.Search();
    };
    // Showing
    self.Message = ko.computed(function () {
        var startIndex = (self.PageIndex() - 1) * self.PageSize() + 1;
        var endIndex = self.PageIndex() * self.PageSize() <= self.TotalCount() ?
            self.PageIndex() * self.PageSize() : self.TotalCount();

        if (self.TotalCount() != 0)
            return "Showing results from " + startIndex + " - " + endIndex + " of the total " + viewModel.TotalCount();
        else
            return "There is no result";
    }, this);

    function FilterData() {
        openProcess();
        var dataPost = {
            "keyword": self.Keyword(),
            "groupId":self.GroupId(),
            "status": self.StatusId(),
            "sortBy": self.SortBy(),
            "orderBy": self.OrderBy(),
            "pageIndex": self.PageIndex(),
            "pageSize": self.PageSize()
        };

        $.ajax({
            type: "POST",
            url: "/Administrator/Users/GetList",
            data: dataPost,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                // Stop progress
                hideProcess();

                // Set values
                self.TotalCount(data.totalCount);
                self.TotalPages(data.totalPages);
                self.UserItems(data.userItems == null ? [] : data.userItems);
            },
            error: function (xhr, status, error) {
                // Stop progress bar
                hideProcess();
                showPopupError();
            }
        });
    }

    // Search
    self.Search = function () {
        FilterData();
    };
    self.ActionSearch = function () {
        self.PageIndex(1);
        FilterData();
    };
    
    //Delete single
    self.DeleteItem = function (item) {
        self.ItemId(item.id);
        bootbox.confirm({
            title: "Notification",
            message: 'Are you sure you want to delete account "' + item.userName +'"?',
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm',
                    className: 'btn-primary'
                },
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    openProcess();
                    $.ajax({
                        type: "POST",
                        url: "/Administrator/Users/Delete",
                        data: { "id": item.id},
                        dataType: "json",
                        success: function (data, textStatus, jqXHR) {
                            // Stop progress
                            hideProcess();
                            if (data.error === true) {
                                showErrorMessage(data.message);
                            } else {
                                showSuccessMessage(data.message);
                                self.Search();
                            }
                        },
                        error: function (xhr, status, error) {
                            // Stop progress bar
                            hideProcess();
                            showPopupError();
                        }
                    });
                    
                }
            }
        });
    }

    //Reset password
    self.ResetPassword = function(item) {
        self.ItemId(item.id);
        bootbox.confirm({
            title: "Notification",
            message: 'Are you sure you want to reset password account "' + item.userName + '"?',
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm',
                    className: 'btn-primary'
                },
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    openProcess();
                    $.ajax({
                        type: "POST",
                        url: "/Administrator/Users/ResetPassword",
                        data: { "id": item.id },
                        dataType: "json",
                        success: function (data, textStatus, jqXHR) {
                            // Stop progress
                            hideProcess();
                            if (data.error === true) {
                                showErrorMessage(data.message);
                            } else {
                                bootbox.alert(data.message);
                            }
                        },
                        error: function (xhr, status, error) {
                            // Stop progress bar
                            hideProcess();
                            showPopupError();
                        }
                    });
                }
            }
        });
    }

    //Assign to group
    self.UserInfo = ko.observable('');
    self.GroupFunctionItems = ko.observableArray([]);
    self.AssignGroup = function (item) {
        self.ItemId(item.id);
        self.UserInfo(item.userName + ' - ' + item.fullName);
        openProcess();
        self.GroupFunctionItems.removeAll();
        $.ajax({
            type: "POST",
            url: "/Administrator/Users/GetRoleByUser",
            data: { "id": item.id},
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                // Stop progress
                hideProcess();
                $.each(data.groupFunctionItems,function() {
                    self.GroupFunctionItems.push({
                        Id: ko.observable(this.id),
                        Name: ko.observable(this.name),
                        IsChecked: ko.observable(this.isChecked)
                    });
                });
                $('#popupUserRole').modal('show');
            },
            error: function (xhr, status, error) {
                // Stop progress bar
                hideProcess();
                showPopupError();
            }
        });
    }

    self.SubmitAssignGroup = function () {
        var groupId = [];
        $.each(self.GroupFunctionItems(), function () {
            if (this.IsChecked() === true) {
                groupId.push(this.Id());
            }
        });
        openProcess();
        $.ajax({
            type: "POST",
            url: "/Administrator/Users/SaveRole",
            data: { "userId": self.ItemId(), "roleIds": groupId },
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                // Stop progress
                hideProcess();
                $('#popupUserRole').modal('hide');
                if (data.error === true) {
                    showErrorMessage(data.message);
                } else {
                    showSuccessMessage(data.message);
                    self.Search();
                }
            },
            error: function (xhr, status, error) {
                // Stop progress bar
                hideProcess();
                showPopupError();
            }
        });
    }
}

ko.applyBindings(viewModel, document.getElementById("divList"));


//// Onload
// jQuery
$(document).ready(function () {
    // First Load
    viewModel.Search();
});