﻿$(document).ready(function () {
    CKEDITOR.replace('Content',
        {
            height: 300,
            filebrowserBrowseUrl: '/administrator/ckfinder/filebrowse',
            filebrowserUploadUrl: '/administrator/ckfinder/UploadFile',
        });
});