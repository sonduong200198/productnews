﻿var validateKey = function (name, id) {
    var validated = true;

    if (name !== undefined && name != null && name !== "") {
        $.ajax({
            async: false,
            type: "POST",
            url: "/Administrator/Permissions/ExistKey",
            data: {
                "id": id,
                "name": name
            },
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                // Stop progress
                if (data !== "")
                    validated = false;
            },
            error: function (xhr, status, error) {

            }
        });
    }
    return validated;
};

var viewModel = new function () {
    var self = this;

    self.ItemId = ko.observable(0);

    self.MenuGroupId = ko.observable(data.functionInfo.menuGroupId);
    self.MenuGroupOptions = ko.observableArray(data.menuGroupOptions != null ? data.menuGroupOptions : []);

    self.ControllerName = ko.observable(data.functionInfo.controllerName).extend({ required: true });
    self.ActionName = ko.observable(data.functionInfo.actionName).extend({ required: true, maxLength: { params: 150, message: "Please enter no more than 150 characters." } });
    self.Name = ko.observable(data.functionInfo.name).extend({ required: true, maxLength: { params: 150, message: "Please enter no more than 150 characters." }  });
    self.Description = ko.observable(data.functionInfo.description);
    self.Icon = ko.observable(data.functionInfo.icon);
    self.OrderBy = ko.observable(1).extend({ number: true});;
    self.Submit = function () {
        if (viewModel.errors().length !== 0) {
            viewModel.errors.showAllMessages();
        } else {
            $('#frmAdd').submit();
        }
    }
}

viewModel.errors = ko.validation.group(viewModel);
ko.validation.rules.pattern.message = 'Invalid.';
ko.applyBindings(viewModel, document.getElementById("divList"));

