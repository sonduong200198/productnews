﻿var viewModel = new function () {
    var self = this;

    self.ItemId = ko.observable(0);
    self.UserId = ko.observable('');
    self.UserOptions = ko.observableArray(data.userOptions != null ? data.userOptions : []);

    self.LogTypeId = ko.observable('');
    self.LogTypeOptions = ko.observableArray(data.logTypeOptions != null ? data.logTypeOptions : []);
    
    self.ActivityLogItems = ko.observableArray([]);

    // Paginate
    self.Keyword = ko.observable('');
    self.FromDate = ko.observable('');
    self.ToDate = ko.observable('');
    self.SortBy = ko.observable('CREATEDATE');
    self.OrderBy = ko.observable('DESC');
    self.PageIndex = ko.observable(1);
    self.PageSize = ko.observable(20);
    self.TotalPages = ko.observable(0);
    self.TotalCount = ko.observable(0);
    self.PageSlide = ko.observable(4);

    self.LastPage = ko.computed(function () {
        return Math.floor((self.TotalCount() - 1) / self.PageSize()) + 1;
    });
    self.Pages = ko.computed(function () {
        var pageCount = self.LastPage();
        var pageFrom = Math.max(1, self.PageIndex() - self.PageSlide());
        var pageTo = Math.min(pageCount, self.PageIndex() + self.PageSlide());
        pageFrom = Math.max(1, Math.min(pageTo - 2 * self.PageSlide(), pageFrom));
        pageTo = Math.min(pageCount, Math.max(pageFrom + 2 * self.PageSlide(), pageTo));

        var result = [];
        for (var i = pageFrom; i <= pageTo; i++) {
            result.push({ pageNumber: (i) });
        }
        return result;
    });
    // Prev | Next page
    self.PrevPage = function () {
        var pageIndex = viewModel.PageIndex();
        if (pageIndex > 1) {
            viewModel.PageIndex(pageIndex - 1);
            viewModel.Search();
        }
    };
    self.NextPage = function () {
        var pageIndex = viewModel.PageIndex();
        if (pageIndex < viewModel.TotalPages()) {
            viewModel.PageIndex(pageIndex + 1);
            viewModel.Search();
        }
    };
    self.MoveToPage = function (index) {
        self.PageIndex(index);
        viewModel.Search();
    };
    // Showing
    self.Message = ko.computed(function () {
        var startIndex = (self.PageIndex() - 1) * self.PageSize() + 1;
        var endIndex = self.PageIndex() * self.PageSize() <= self.TotalCount() ?
            self.PageIndex() * self.PageSize() : self.TotalCount();

        if (self.TotalCount() != 0)
            return "Showing results from " + startIndex + " - " + endIndex + " of the total " + viewModel.TotalCount();
        else
            return "There is no result";
    }, this);
    

    function FilterData() {
        openProcess();
        var dataPost = {
            "keyword": self.Keyword(),
            "userId":self.UserId(),
            "type": self.LogTypeId(),
            "from": self.FromDate(),
            "to": self.ToDate(),
            "sortBy": self.SortBy(),
            "orderBy": self.OrderBy(),
            "pageIndex": self.PageIndex(),
            "pageSize": self.PageSize()
        };

        $.ajax({
            type: "POST",
            url: "/Administrator/Activities/GetList",
            data: dataPost,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                // Stop progress
                hideProcess();

                // Set values
                self.TotalCount(data.totalCount);
                self.TotalPages(data.totalPages);
                self.ActivityLogItems(data.activityLogItems != null ? data.activityLogItems : []);
            },
            error: function (xhr, status, error) {
                // Stop progress bar
                hideProcess();
                showPopupError();
            }
        });
    }

    // Search
    self.Search = function () {
        FilterData();
    };
    self.ActionSearch = function () {
        self.PageIndex(1);
        FilterData();
    };

    //Detail
    self.LogDetailInfo = ko.observable('');
    self.ViewDetail = function(item) {
        self.LogDetailInfo(item);
        console.log(self.LogDetailInfo())
        $('#popupActivityLogDetail').modal('show');
    }
}

ko.applyBindings(viewModel, document.getElementById("divList"));

//// Onload
// jQuery
$(document).ready(function () {
    // First Load
    viewModel.Search();
});