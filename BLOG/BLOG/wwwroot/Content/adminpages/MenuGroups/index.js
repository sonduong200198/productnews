﻿var viewModel = new function () {
    var self = this;

    self.ItemId = ko.observable(0);
    self.MenuGroupItems = ko.observableArray([]);

    // Paginate
    self.Keyword = ko.observable('');   
    self.SortBy = ko.observable('NAME');
    self.OrderBy = ko.observable('ASC');
    self.PageIndex = ko.observable(1);
    self.PageSize = ko.observable(10);
    self.TotalPages = ko.observable(0);
    self.TotalCount = ko.observable(0);
    self.PageSlide = ko.observable(4);

    self.LastPage = ko.computed(function () {
        return Math.floor((self.TotalCount() - 1) / self.PageSize()) + 1;
    });
    self.Pages = ko.computed(function () {
        var pageCount = self.LastPage();
        var pageFrom = Math.max(1, self.PageIndex() - self.PageSlide());
        var pageTo = Math.min(pageCount, self.PageIndex() + self.PageSlide());
        pageFrom = Math.max(1, Math.min(pageTo - 2 * self.PageSlide(), pageFrom));
        pageTo = Math.min(pageCount, Math.max(pageFrom + 2 * self.PageSlide(), pageTo));

        var result = [];
        for (var i = pageFrom; i <= pageTo; i++) {
            result.push({ pageNumber: (i) });
        }
        return result;
    });
    // Prev | Next page
    self.PrevPage = function () {
        var pageIndex = self.PageIndex();
        if (pageIndex > 1) {
            self.PageIndex(pageIndex - 1);
            self.Search();
        }
    };
    self.NextPage = function () {
        var pageIndex = self.PageIndex();
        if (pageIndex < self.TotalPages()) {
            self.PageIndex(pageIndex + 1);
            self.Search();
        }
    };
    self.MoveToPage = function (index) {
        self.PageIndex(index);
        self.Search();
    };
    // Showing
    self.Message = ko.computed(function () {
        var startIndex = (self.PageIndex() - 1) * self.PageSize() + 1;
        var endIndex = self.PageIndex() * self.PageSize() <= self.TotalCount() ?
            self.PageIndex() * self.PageSize() : self.TotalCount();

        if (self.TotalCount() != 0)
            return "Showing results from " + startIndex + " - " + endIndex + " of the total " + self.TotalCount();
        else
            return "There is no result";
    }, this);

    function FilterData() {
        openProcess();
        var dataPost = {
            "keyword": self.Keyword(),
            "sortBy": self.SortBy(),
            "orderBy": self.OrderBy(),
            "pageIndex": self.PageIndex(),
            "pageSize": self.PageSize()
        };

        $.ajax({
            type: "POST",
            url: "/Administrator/MenuGroups/GetList",
            data: dataPost,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                // Stop progress
                hideProcess();

                // Set values
                self.TotalCount(data.totalCount);
                self.TotalPages(data.totalPages);
                self.MenuGroupItems(data.menuGroupItems == null ? [] : data.menuGroupItems);
            },
            error: function (xhr, status, error) {
                // Stop progress bar
                hideProcess();
                showPopupError();
            }
        });
    }

    // Search
    self.Search = function () {
        FilterData();
    };
    self.ActionSearch = function () {
        self.PageIndex(1);
        FilterData();
    };

    self.AddItem = function () {
        frmItemModel.ItemId(0);
        frmItemModel.Mode('CREATE');
        frmItemModel.Name('');
        frmItemModel.Icon('');
        frmItemModel.Description('');
        $('#popupMenuGroupFrmItem').modal('show');
    }
    //Delete single
    self.DeleteItem = function (item) {
        self.ItemId(item.id);
        bootbox.confirm({
            title: "Notification",
            message: 'Are you sure you want to delete Menu group "' + item.name +'"?',
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm',
                    className: 'btn-primary'
                },
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    openProcess();
                    $.ajax({
                        type: "POST",
                        url: "/Administrator/MenuGroups/Delete",
                        data: { "id": item.id},
                        dataType: "json",
                        success: function (data, textStatus, jqXHR) {
                            // Stop progress
                            hideProcess();
                            if (data.error === true) {
                                showErrorMessage(data.message);
                            } else {
                                showSuccessMessage(data.message);
                                self.Search();
                            }
                        },
                        error: function (xhr, status, error) {
                            // Stop progress bar
                            hideProcess();
                            showPopupError();
                        }
                    });
                    
                }
            }
        });
    }

    self.EditItem = function (item) {
        frmItemModel.ItemId(item.id);
        frmItemModel.Mode('UPDATE');
        frmItemModel.Name(item.name);
        frmItemModel.Icon(item.icon);
        frmItemModel.Description(item.description);
        $('#popupMenuGroupFrmItem').modal('show');
    }
}

ko.applyBindings(viewModel, document.getElementById("divList"));

var frmItemModel = new function () {
    var self = this;

    self.ItemId = ko.observable(0);
    self.Mode = ko.observable('CREATE');
    self.Name = ko.observable('').extend({ required: true });
    self.Icon = ko.observable('');
    self.Description = ko.observable('');
    
    self.Submit = function () {
        if (frmItemModel.errors().length !== 0) {
            frmItemModel.errors.showAllMessages();
        } else {
            var menuGroupInfo = {
                Id:self.ItemId(),
                Name: self.Name(),
                Icon: self.Icon(),
                Description: self.Description()
            }
            openProcess();
            $.ajax({
                type: "POST",
                url: "/Administrator/MenuGroups/UpdateData",
                data: { item: menuGroupInfo},
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    // Stop progress
                    hideProcess();
                    if (data.error === true) {
                        showErrorMessage(data.message);
                    } else {
                        $('#popupMenuGroupFrmItem').modal('hide');
                        showSuccessMessage(data.message);
                        viewModel.Search();
                    }
                },
                error: function (xhr, status, error) {
                    // Stop progress bar
                    hideProcess();
                    showPopupError();
                }
            });
        }
    }
}
frmItemModel.errors = ko.validation.group(frmItemModel);
ko.validation.rules.pattern.message = 'Invalid.';
ko.applyBindings(frmItemModel, document.getElementById("popupMenuGroupFrmItem"));
//// Onload
// jQuery
$(document).ready(function () {
    // First Load
    viewModel.Search();
});