function isElementInViewport(el) {
    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }
    var rect = el.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}
function loadLazy(selector, callback) {
    if (!$(selector).length) return;
    $(window).on('DOMContentLoaded load resize scroll', [selector, callback], loadLazyAppend);
}
function loadLazyAppend(e) {
    var selector = e.data[0];
    var callback = e.data[1];

    var appended = $(selector).attr('data-appended');
    if (appended) return;
    if (document.documentElement.scrollTop + document.body.scrollTop > 0) {
        $(selector).attr('data-appended', true);
        if (callback) callback();
        return;
    }

    var visible = isElementInViewport($(selector));
    if (visible) {
        $(selector).attr('data-appended', true);
        if (callback) callback();
    }
}

loadLazy('.load_img', loadLazyImage);
function loadLazyImage() {
    console.log('Loaded image');
    $('img[data-src]').each(function () {
        var data = $(this).data();
        $(this).attr('src', data.src);
    })
}
$(document).on('click', '.tab-button', function () {
    $('.tab-button').removeClass('active');
    $(this).addClass('active');
    $('.tab-item').hide();
    var tabContent = $(this).attr('data-tab');
    $('#' + tabContent).show();
});
function searchFaceKeypress(searchUrl) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        searchFaceClick(searchUrl);
    }
}
function searchFaceClick(searchUrl) {
    var valueInput = $(".search-face-input").val();
    var url = searchUrl;
    if (valueInput != null && valueInput != "") {
        url += "?keyword=" + valueInput;
    }
    window.location.href = url;
}

function searchFaceKeypress1(searchUrl) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        searchFaceClick1(searchUrl);
    }
}
function searchFaceClick1(searchUrl) {
    var valueInput = $(".search-face-input1").val();
    var url = searchUrl;
    if (valueInput != null && valueInput != "") {
        url += "?keyword=" + valueInput;
    }
    window.location.href = url;
}
function openPopupForm(link) {

    var modal1 = $('#modal-placeholder-Fronend .modal.show');
    var url = $(event.target).data('url');
    if (link) url = link;

    var placeholderElement = $('#modal-placeholder-Fronend');

    $.get(url).done(function (data) {
        placeholderElement.html(data);
        placeholderElement.find('.modal').modal('show');
        var form = placeholderElement.find('form');
        $.validator.unobtrusive.parse(form);

    });
}
function savePopupFormFront() {
    event.preventDefault();

    var form = $(event.target).parents('.modal').find('form')[0];
    var isValid = $(form).valid();
    if (!isValid) return;

    var submitButton = $(event.target).find('[type="submit"]');
    submitButton.html(submitButton.attr('data-text'));
    submitButton.attr("disabled", true);

    var actionUrl = $(event.target).parents('.modal').find('form').attr('action');
    var formData = new FormData(form);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: actionUrl,
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
            var placeholderElement = $('#modal-placeholder-Fronend');
            placeholderElement.find('.modal').modal('hide');
            //showSuccessMessage("Successful implementation!");
            $(".message-form-request").css("display", "flex");
            setTimeout(function () {
                $(".message-form-request").fadeOut("slow");
            }, 4000);
            //$(".message-form-request").css({ "display","none" });
        }
    });
    }
//test save category child
function reloadPage() {
    var refreshArea = $('[data-url-refresh]').last();
    var url = refreshArea.attr('data-url-refresh');
    $.get(url).done(function (data) {
        var refreshedData = $(data).find('[data-url-refresh]').html();
        refreshArea.html(refreshedData);
    });
}
function showSuccessMessage(message, center, timeOut) {
    if (!timeOut) {
        timeOut = 2000;
    }
    var centerClass = "gritter-bottom ";
    if (center == null || !center)
        centerClass = "";

    $.gritter.add({
        position: 'bottom-right',
        time: timeOut,
        title: '<i class="fa fa-check-circle"></i>  Message',
        text: message,
        class_name: centerClass + 'gritter-info'
    });

    $('.alert-dismissable').remove();
};
$(document).ready(function () {
    $("form").each(function () {
        $(this).validate({
            validateDelegate: function () { },
            onsubmit: true,
            onkeydown: false,
            onkeyup: false,
            onfocusin: false,
            onfocusout: false,
        });
    });
});

function clearText(name) {
    $('.' + name).val('');
    $('.' + name).focus();
}
function getParams() {
    var params = {};
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) { params[key] = value; });
    return params;
}
$(document).ready(function () {
    var params = getParams();
    var keywords = decodeURI(params.keyword);
    if (keywords !== 'undefined') $('.keywords').val(keywords);
});