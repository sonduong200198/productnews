function isElementInViewport(el) {
    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }
    var rect = el.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}
function loadLazy(selector, callback) {
    if (!$(selector).length) return;
    $(window).on('DOMContentLoaded load resize scroll', [selector, callback], loadLazyAppend);
}
function loadLazyAppend(e) {
    var selector = e.data[0];
    var callback = e.data[1];

    var appended = $(selector).attr('data-appended');
    if (appended) return;
    if (document.documentElement.scrollTop + document.body.scrollTop > 0) {
        $(selector).attr('data-appended', true);
        if (callback) callback();
        return;
    }

    var visible = isElementInViewport($(selector));
    if (visible) {
        $(selector).attr('data-appended', true);
        if (callback) callback();
    }
}

loadLazy('.load_img', loadLazyImage);
function loadLazyImage() {
    console.log('Loaded image');
    $('img[data-src]').each(function () {
        var data = $(this).data();
        $(this).attr('src', data.src);
    })
}
$(document).on('click', '.tab-button', function () {
    $('.tab-button').removeClass('active');
    $(this).addClass('active');
    $('.tab-item').hide();
    var tabContent = $(this).attr('data-tab');
    $('#' + tabContent).show();
});