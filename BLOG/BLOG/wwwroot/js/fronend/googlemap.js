var mapDiv = document.getElementById('map-canvas');
if (mapDiv) {
    appendContactMap();
}
function appendContactMap() {
    var contactKeys = $('#contact-keys').data();
    if (!contactKeys) return;
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&callback=initialize&key=' + contactKeys.apiKey + '';
    document.body.appendChild(script);
    console.log('Loaded');
}
function initialize() {
    var mapDiv = document.getElementById('map-canvas');

    var mapOptions = {
        zoom: 16,
        minzoom: 1,
        mapTypeControl: true,
        zoomControl: true,
        scaleControl: true,
        streetViewControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    window.map = new google.maps.Map(mapDiv, mapOptions);
    updateMap();


    // Setup for second map
    mapOptions.center = new google.maps.LatLng(10.783308, 106.696770); // CN SAI GON
    map2 = new google.maps.Map(document.getElementById("map-canvas2"), mapOptions);

    var name = $('.branch-saigon h5').text(); 
    var address = $('.branch-saigon .home-address').text();

    var infowindow2 = new google.maps.InfoWindow();
    infowindow2.setContent('<div style="max-width:200px"><strong>' + name + '</strong><br>' + address + '</div>');
    var currentMarker = new google.maps.Marker({
        position: mapOptions.center,
        map: map2,
        draggable: true
    });
    currentMarker.setPosition(mapOptions.center);
    infowindow2.open(map2, currentMarker);
};

function updateMap(e) {
    var geocoder = new google.maps.Geocoder;
    var address = $('[data-address]').data("address");
    var dataPosition = "20.998748, 105.798035";
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK || true) {
            //var position = results[0].geometry.location;
            //20.998748, 105.798035 10.783308, 106.696770
            var position = "20.998748, 105.798035";
            var arr = dataPosition.split(', ');
            position = new google.maps.LatLng(arr[0], arr[1]);
            window.position = position;
            map.setCenter(position);
            setInfowindow();
        }
    });
}

function setInfowindow() {
    var name = $('.branch-hanoi h5').text();
    var address = $('.branch-hanoi .home-address').text();
    var positionPoint = "20.998748, 105.798035";
    var mapCurrent = window.map;

    var arr = positionPoint.split(', ');
    positionCurrent = new google.maps.LatLng(arr[0], arr[1]);
    if (window.infowindow) window.infowindow.close();
    window.infowindow = new google.maps.InfoWindow();
    infowindow.setContent('<div style="max-width:200px"><strong>' + name + '</strong><br>' + address + '</div>');
    if (!window.currentMarker) {
        window.currentMarker = new google.maps.Marker({
            position: positionCurrent,
            map: mapCurrent,
            draggable: true
        });
    }
    window.currentMarker.setPosition(window.position);
    infowindow.open(window.map, window.currentMarker);
}