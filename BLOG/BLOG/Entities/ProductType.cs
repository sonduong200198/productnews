﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities
{
    public partial class ProductType
    {
        public int Id { get; set; }
        public int? Price { get; set; }
        public int? Promotion { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public int? ProductId { get; set; }
    }
}
