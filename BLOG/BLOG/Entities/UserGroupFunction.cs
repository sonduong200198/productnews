﻿using System;
using System.Collections.Generic;

namespace BLOG.Entities
{
    public partial class UserGroupFunction
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? GroupFunctionId { get; set; }

        public SysGroupFunction GroupFunction { get; set; }
        public User User { get; set; }
    }
}
