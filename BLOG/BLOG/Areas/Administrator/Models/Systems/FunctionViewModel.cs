﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using BLOG.Models.Sys;

namespace BLOG.Areas.Administrator.Models.Systems
{
    public class FunctionViewModel : BaseViewModel
    {
        public FunctionViewModel()
        {
            GroupFunctionItems = new List<SysGroupFunctionModel>();
            FunctionItems= new List<SysFunctionsModel>();
            FunctionInfo = new SysFunctionsModel();
            MenuGroupOptions = new List<SelectListItem>();
        }
        public List<SysGroupFunctionModel> GroupFunctionItems { get; set; }

        public List<SysFunctionsModel> FunctionItems { get; set; }

        public SysFunctionsModel FunctionInfo { get; set; }

        public List<SelectListItem> MenuGroupOptions { get; set; }
    }
    
}
