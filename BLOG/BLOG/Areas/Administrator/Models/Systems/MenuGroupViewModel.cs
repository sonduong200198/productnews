﻿using System.Collections.Generic;
using BLOG.Models.Sys;

namespace BLOG.Areas.Administrator.Models.Systems
{
    public class MenuGroupViewModel : BaseViewModel
    {
        public MenuGroupViewModel()
        {
            MenuGroupItems = new List<SysMenuGroupModel>();
            MenuGroupInfo = new SysMenuGroupModel();
        }
        public List<SysMenuGroupModel> MenuGroupItems { get; set; }

        public SysMenuGroupModel MenuGroupInfo { get; set; }
    }
}
