﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using BLOG.Models.Sys;
using BLOG.Models.Users;

namespace BLOG.Areas.Administrator.Models.Users
{
    public class UserViewModel : BaseViewModel
    {
        public UserViewModel()
        {
            StatusOptions = new List<SelectListItem>();
            GroupOptions = new List<SelectListItem>();
            UserItems = new List<UserModel>();
            UserInfo = new UserModel();
            GroupFunctionItems =new List<SysGroupFunctionModel>();
        }
        public List<SelectListItem> StatusOptions { get; set; }

        public List<SelectListItem> GroupOptions { get; set; }

        public List<UserModel> UserItems { get; set; }

        public UserModel UserInfo { get; set; }

        public List<SysGroupFunctionModel> GroupFunctionItems { get; set; }
    }

    public class ResetPasswordViewModel : BaseViewModel
    {
        [Required]
        [Display(Name = "Current password")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(16, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
