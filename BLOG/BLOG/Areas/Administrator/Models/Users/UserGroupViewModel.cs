﻿using System.Collections.Generic;
using BLOG.Models.Sys;

namespace BLOG.Areas.Administrator.Models.Users
{
    public class UserGroupViewModel :BaseViewModel
    {
        public UserGroupViewModel()
        {
            GroupFunctionItems = new List<SysGroupFunctionModel>();
            GroupFunctionInfo = new SysGroupFunctionModel();
        }
        public List<SysGroupFunctionModel> GroupFunctionItems { get; set; }

        public SysGroupFunctionModel GroupFunctionInfo { get; set; }
    }
}
