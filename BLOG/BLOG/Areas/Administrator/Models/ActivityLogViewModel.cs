﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using BLOG.Models.Sys;

namespace BLOG.Areas.Administrator.Models
{
    public class ActivityLogViewModel : BaseViewModel
    {
        public ActivityLogViewModel()
        {
            LogTypeOptions = new List<SelectListItem>();
            UserOptions = new List<SelectListItem>();
            ActivityLogInfo = new SysActivityLogModel();
            ActivityLogItems = new List<SysActivityLogModel>();
        }
        public List<SelectListItem> LogTypeOptions { get; set; }
        public List<SelectListItem> UserOptions { get; set; }
        public SysActivityLogModel ActivityLogInfo { get; set; }
        public List<SysActivityLogModel> ActivityLogItems { get; set; }
    }
}
