﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    public class CkFinderController : BaseController
    {

        /// <summary>
        /// Path upload file use at the config file
        /// </summary>
        IHostingEnvironment _hostingEnviroment;
        private IConfiguration _configuration;
        private string pathCKFinder = "";
        public CkFinderController(IHostingEnvironment hostingEnvironment, IConfiguration Configuration)
        {
            _hostingEnviroment = hostingEnvironment;
            _configuration = Configuration;
            pathCKFinder =_configuration.GetSection("PathCKFinder").Value;
        }


        /// <summary>
        /// Example
        /// Author: Quyetqv
        /// Create date: 2020-0212
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        public IActionResult Index()
        {
            var path = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory(), _hostingEnviroment.WebRootPath, pathCKFinder));
            ViewBag.FilesInfo = path.GetFiles();
            return View();
        }

        /// <summary>
        /// Get infor file upload forder width ckfinder
        /// Author: Quyetqv
        /// Create date: 2020-0212
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FileBrowse()
        {
            try
            {
                var path = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory(), _hostingEnviroment.WebRootPath, pathCKFinder));
                ViewBag.FilesInfo = path.GetFiles();
                return View();
            }
            catch (Exception ex)
            {
                ViewData["Message"] = ex.Message;
                return View();
            }

        }

        /// <summary>
        /// Use upload file form data
        /// Author: Quyetqv
        /// Create date: 2020-0212
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult FileBrowse(IFormFile upload)
        {
            try
            {
                if (upload == null)
                    return View();
                var filename = DateTime.Now.ToString("yyyyMMddHHmmss") + upload.FileName;
                var path = Path.Combine(Directory.GetCurrentDirectory(), _hostingEnviroment.WebRootPath, pathCKFinder, filename);
                var stream = new FileStream(path, FileMode.Create);
                upload.CopyToAsync(stream);
                var pathinfo = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory(), _hostingEnviroment.WebRootPath, pathCKFinder));
                ViewBag.FilesInfo = pathinfo.GetFiles();
                return View();
            }
            catch (Exception ex)
            {
                ViewData["Message"] = ex.Message;
                return View();
            }
        }

        /// <summary>
        /// Use upload file ajax
        /// Author: Quyetqv
        /// Create date: 2020-0212
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        public IActionResult UploadFile(IFormFile upload)
        {
            try
            {
                if (upload == null)
                    return new JsonResult(new { Status = "Error", Message = "File upload is null" });
                var filename = DateTime.Now.ToString("yyyyMMddHHmmss") + upload.FileName;
                var path = Path.Combine(Directory.GetCurrentDirectory(), _hostingEnviroment.WebRootPath, pathCKFinder, filename);
                var stream = new FileStream(path, FileMode.Create);
                upload.CopyToAsync(stream);

                return new JsonResult(new { Status = "Success", Message = "Upload file success", fileName = filename, url = "/"+ pathCKFinder + "/" + filename, uploaded = 1 });
            }
            catch (Exception ex)
            {
                return new JsonResult(new { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Delete of ckfinder
        /// Author: Quyetqv
        /// Create date: 2020-0212
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DeleteBrowse(string fileName)
        {
            try
            {
                var path = _hostingEnviroment.WebRootPath + fileName;
                System.IO.File.Delete(path);


                var getpath = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory(), _hostingEnviroment.WebRootPath, pathCKFinder));
                ViewBag.FilesInfo = getpath.GetFiles();
                return new JsonResult(new { Status = "Success", Code = 200, Message = "Delete file success" });
            }
            catch (Exception ex)
            {
                var path = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory(), _hostingEnviroment.WebRootPath, pathCKFinder));
                ViewBag.FilesInfo = path.GetFiles();
                ViewData["Message"] = ex.Message;
                return new JsonResult(new { Status = "Error", Code = 500, Message = ex.Message });
            }

        }
    }
}