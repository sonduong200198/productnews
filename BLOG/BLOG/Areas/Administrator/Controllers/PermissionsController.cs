﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using BLOG.App_Core.Attributes;
using BLOG.App_Core.Extensions;
using BLOG.Areas.Administrator.Models.Systems;
using BLOG.Business.Sys;
using BLOG.Core.Constants;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Sys;

namespace BLOG.Areas.Administrator.Controllers
{
    
    [Area("Administrator")]
    [UserAuthorize]
    public class PermissionsController : BaseController
    {
        FunctionViewModel model = new FunctionViewModel();
        private readonly ISysFunctionsService _functionsService;
        private readonly ISysGroupFunctionService _groupFunctionService;
        private readonly ISysFunctionGroupFunctionService _functionGroupFunctionService;
        private readonly ISysMenuGroupService _menuGroupService;

        public PermissionsController(ISysFunctionsService functionsService, ISysGroupFunctionService groupFunctionService,
            ISysFunctionGroupFunctionService functionGroupFunctionService, ISysMenuGroupService menuGroupService)
        {
            _functionsService = functionsService;
            _groupFunctionService = groupFunctionService;
            _functionGroupFunctionService = functionGroupFunctionService;
            _menuGroupService = menuGroupService;
        }

        #region #Index

        public IActionResult Index()
        {
            model.GroupFunctionItems = _groupFunctionService.GetAll().MapTo(new List<SysGroupFunctionModel>()).ToList();
            model.FunctionItems = _functionsService.GetAll().MapTo(new List<SysFunctionsModel>()).ToList();
            foreach (var item in model.GroupFunctionItems)
            {
                item.FunctionIds = _functionGroupFunctionService.FindAllBy(m => m.GroupFunctionId == item.Id).Select(c => NumberTools.ConvertToInt(c.FunctionId)).ToList();
            }
            return View(model);
        }

        #endregion

        #region #Add

        public IActionResult Add()
        {
            model.MenuGroupOptions = GetMenuGroupOptions(false);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(SysFunctionsModel item)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.FunctionInfo = item;
                    model.MenuGroupOptions = GetMenuGroupOptions(false);
                    TempData[Constants.TempDataKey.Error] = Constants.Message.ValidateFail;
                    return View(model);
                }
                item = item.TrimStringProperties();
                item.Status = Constants.Status.Active;
                item.ActionName = string.IsNullOrEmpty(item.ActionName) ? "Index" : item.ActionName;
                item.CreatedBy = GetUserId(User.Identity);
                item.ShowOnMenu = true;
                _functionsService.Add(item.MapTo<SysFunctionsModel, SysFunction>());
                _functionsService.Save();
                TempData[Constants.TempDataKey.Success] = string.Format(Constants.Message.SuccessToCreate, "Permissions");
                //Log
                AddActivityLog(this.HttpContext, Constants.LogType.Insert, Constants.ResourceType.Admin,
                    string.Format(Constants.MessageLog.Insert, "Permissions", SerializeTool.Serialize(item.MapTo<SysFunctionsModel, SysFunction>())));
                return RedirectToAction("Index");
            }
            catch
            {
                TempData[Constants.TempDataKey.Error] = string.Format(Constants.Message.FailToCreate, "Permissions");
                return RedirectToAction("Add");
            }
        }

        #endregion

        #region #Edit

        public IActionResult Edit(int id)
        {
            var functionObj = _functionsService.Find(id);
            if(functionObj==null)
                return RedirectToAction("Index");
            model.FunctionInfo = functionObj.MapTo<SysFunction,SysFunctionsModel>();
            model.MenuGroupOptions = GetMenuGroupOptions(false);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, SysFunctionsModel item)
        {
            try
            {
                var functionObj = _functionsService.FirstOrDefault(m => m.Id == id);
                if (functionObj == null)
                    return RedirectToAction("Index");

                if (!ModelState.IsValid)
                {
                    model.FunctionInfo = item;
                    model.MenuGroupOptions = GetMenuGroupOptions(false);
                    TempData[Constants.TempDataKey.Error] = Constants.Message.ValidateFail;
                    return View(model);
                }
                item = item.TrimStringProperties();
                functionObj.ControllerName = item.ControllerName;
                functionObj.ActionName = item.ActionName;
                functionObj.Name = item.Name;
                functionObj.Description = item.Description;
                functionObj.MenuGroupId = item.MenuGroupId;
                functionObj.Icon = item.Icon;
                functionObj.OrderBy = item.OrderBy;
                functionObj.UpdatedDate = DateTime.Now;
                functionObj.UpdatedBy = GetUserId(User.Identity);
                _functionsService.Edit(functionObj);
                _functionsService.Save();
                TempData[Constants.TempDataKey.Success] = string.Format(Constants.Message.SuccessToUpdate, "Permissions");
                //Log
                AddActivityLog(this.HttpContext, Constants.LogType.Update, Constants.ResourceType.Admin,
                    string.Format(Constants.MessageLog.Update, "Permissions", SerializeTool.Serialize(functionObj)));
                return RedirectToAction("Index");
            }
            catch
            {
                TempData[Constants.TempDataKey.Error] = string.Format(Constants.Message.FailToUpdate, "Permissions");
                return RedirectToAction("Index");
            }
        }
        #endregion

        #region #Update permission
        [HttpPost]
        public IActionResult UpdatePermissions(string roles)
        {
            try
            {
                var groups = SerializeTool.Deserialize<List<SysGroupFunctionModel>>(roles);
                var result = _functionsService.UpdatePermissions(groups);
                if (result.Success)
                {
                    model.Error = false;
                    model.Message = string.Format(Constants.Message.SuccessToUpdate, "Permissions");
                    //Log
                    AddActivityLog(this.HttpContext, Constants.LogType.Update, Constants.ResourceType.Admin,
                        string.Format(Constants.MessageLog.Update, "Permissions assign group", SerializeTool.Serialize(groups)));
                }
                else
                {
                    model.Error = true;
                    model.Message = string.Format(Constants.Message.FailToUpdate, "Permissions");
                }
            }
            catch 
            {
                model.Error = true;
                model.Message = string.Format(Constants.Message.FailToUpdate, "Permissions");
            }
            
            return Json(model);
        }

        #endregion

        #region #Delete

        [HttpPost]
        public IActionResult Delete(int id)
        {
            try
            {
                var functionObj = _functionsService.FirstOrDefault(m => m.Id == id);
                if (functionObj == null)
                {
                    model.Error = true;
                    model.Message = string.Format(Constants.Message.IsNotExists, "Permissions");
                }
                else
                {
                    string logContent = string.Format(Constants.MessageLog.Delete, "Permissions", SerializeTool.Serialize(functionObj));
                    bool result = _functionsService.Delete(id);
                    if (result)
                    {
                        model.Error = false;
                        model.Message = string.Format(Constants.Message.SuccessToDelete, "Permissions");
                        TempData[Constants.TempDataKey.Success] = string.Format(Constants.Message.SuccessToDelete, "Permissions");
                        //Log
                        AddActivityLog(this.HttpContext, Constants.LogType.Delete, Constants.ResourceType.Admin, logContent);
                    }
                    else
                    {
                        model.Error = true;
                        model.Message = Constants.Message.ApplicationError;
                    }
                }
            }
            catch
            {
                model.Error = true;
                model.Message = Constants.Message.ApplicationError;
            }
            return Json(model);
        }

        #endregion

        #region #Validate
        [HttpPost]
        public IActionResult ExistKey(int id, string name)
        {
            var obj = _functionsService.FirstOrDefault(m => m.ControllerName == name);
            if (obj != null && obj.Id != id)
                return Json("existed");
            return Json(string.Empty);
        }

        #endregion

        #region #Options

        private List<SelectListItem> GetMenuGroupOptions(bool all)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            if(all)
                result.Add(new SelectListItem("Select menu group","0"));
            var menuGroups = _menuGroupService.GetAll().ToList();
            foreach (var item in menuGroups)
            {
                result.Add(new SelectListItem(item.Name, item.Id.ToString()));
            }

            return result;
        }

        #endregion
    }
}