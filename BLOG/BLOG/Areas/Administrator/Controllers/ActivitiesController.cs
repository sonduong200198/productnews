﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using BLOG.App_Core.Attributes;
using BLOG.App_Core.Extensions;
using BLOG.Areas.Administrator.Models;
using BLOG.Business.Sys;
using BLOG.Business.Users;
using BLOG.Core.Filters;
using BLOG.Core.Utility;
using BLOG.Models.Sys;
using BLOG.Areas.Administrator.Models;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    [UserAuthorize]
    public class ActivitiesController : BaseController
    {
        ActivityLogViewModel model = new ActivityLogViewModel();
        private readonly ISysActivityLogService _sysActivityLogService;
        private readonly IUserService _userService;

        public ActivitiesController(ISysActivityLogService sysActivityLogService, IUserService userService)
        {
            _sysActivityLogService = sysActivityLogService;
            _userService = userService;
        }

        #region #Index

        public IActionResult Index()
        {
            model.LogTypeOptions = GetLogTypeOptions(true);
            model.UserOptions = GetUserOptions(true);
            return View(model);
        }

        /// <summary>
        /// Get list activity logs
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="userId"></param>
        /// <param name="type">Log type: Insert, update , delete ...</param>
        /// <param name="from">DateTime : From Date</param>
        /// <param name="to">DateTime : To Date</param>
        /// <param name="sortBy"></param>
        /// <param name="orderBy"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetList(string keyword, int? userId, string type, string from, string to, string sortBy, string orderBy, int pageIndex = 1, int pageSize = 10)
        {
            DateTime? fromDate = DateTimeTools.CFToDateTime(from);
            DateTime? toDate = DateTimeTools.CFToDateTime(to);

            GlobalParamFilter filters = new GlobalParamFilter
            {
                UserId = userId,
                Keyword = string.IsNullOrEmpty(keyword) ? string.Empty : keyword.Trim(),
                Type = type.Equals("ALL",StringComparison.OrdinalIgnoreCase) ?string.Empty : type,
                StartDate = fromDate?.StartOfDay(),
                EndDate = toDate?.EndOfDay(),
                OrderBy = orderBy,
                SortBy = sortBy,
                PageIndex = pageIndex,
                PageSize = pageSize > 0 ? pageSize : int.MaxValue
            };
            var logItems = await _sysActivityLogService.GetAll(filters);

            if (logItems.Any())
            {
                model.ActivityLogItems = logItems.MapTo(new List<SysActivityLogModel>()).ToList();
                foreach (var item in model.ActivityLogItems)
                {
                    var userObj = _userService.FirstOrDefault(m => m.Id == item.UserId);
                    item.UserName = $"{userObj?.FirstName} {userObj?.LastName}";
                    item.CommentShort = item.Comment.Length > 150 ? item.Comment.Substring(0, 150) + "..." : item.Comment;
                    item.Comment = item.Comment.Replace(@"\r", "").Replace(@"\n", "").Replace(@"\t", "");
                }
            }
            model.PageIndex = pageIndex;
            model.PageSize = pageSize;
            model.TotalPages = logItems.TotalPages;
            model.TotalCount = logItems.TotalCount;
            return Json(model);
        }

        #endregion

        #region #Options

        private List<SelectListItem> GetUserOptions(bool all)
        {
            List<SelectListItem> opt = new List<SelectListItem>();
            if (all)
                opt.Add(new SelectListItem("Select user--", string.Empty));
            var users = _userService.GetAll().OrderBy(m=>m.UserName).ToList();
            foreach (var user in users)
            {
                opt.Add(new SelectListItem
                {
                    Text = $"{user.UserName} - {user.FirstName} {user.LastName}",
                    Value= user.Id.ToString()
                });
            }
            return opt;
        }

        #endregion
    }
}