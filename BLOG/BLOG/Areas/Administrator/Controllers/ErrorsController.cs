﻿using Microsoft.AspNetCore.Mvc;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    public class ErrorsController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AccessDenied()
        {
            return View();
        }
    }
}