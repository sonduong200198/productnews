﻿//using AutoMapper;
//using Microsoft.AspNetCore.Mvc;
//using BLOG.App_Core.Attributes;
//using BLOG.Business.Categories;
//using BLOG.Core.Constants;
//using BLOG.Core.Filters;

//namespace BLOG.Areas.Administrator.Controllers
//{
//    [Area("Administrator")]
//    [UserAuthorize]
//    public class ManagerController : BaseController
//    {
      

//        public ManagerController(IMapper mapper, ICategoryService categoryService)
//        {
//            //_mapper = mapper;
//            //_categoryService = categoryService;
//            //_subItemService = subItemService;
//            //_pageService = pageService;
         
//        }

//        public IActionResult Pages(string sortBy, string orderBy, string keyword, int pageIndex = 1, int pageSize = 20, string type = "")
//        {
//            GlobalParamFilter filters = new GlobalParamFilter
//            {
//                OrderBy = orderBy,
//                SortBy = sortBy,
//                PageIndex = pageIndex,
//                PageSize = pageSize > 0 ? pageSize : int.MaxValue,
//                Keyword = keyword,
//                Type = type
//            };

//            var model = _pieceService.SearchPieces(filters);
//            return View(model);
//        }


//        public IActionResult PieceEditor(string id)
//        {
//            var model = _pieceService.GetPieceModel(id);
//            return View(model);
//        }

//        [HttpPost]
//        public IActionResult PieceEditor(PieceModel model)
//        {
//            if (!ModelState.IsValid) return View(model);

//            // model.UpdatedBy = GetUserId(User.Identity);
//            var save = _pieceService.SavePiece(model);
//            if (!save.Success)
//            {
//                ModelState.AddModelError(save.Data + string.Empty, save.Message);
//                return View(model);
//            }
//            AddActivityLog(this.HttpContext, Constants.LogType.Update, Constants.ResourceType.Admin, save.Message);
//            return View(model);
//        }

//        public IActionResult SubPieceEditor(int id, string text)
//        {
//            var model = _pieceService.GetSubPieceModel(id);
//            if (model.Id == 0) model.ParentId = text;
//            return View(model);
//        }

//        [HttpPost]
//        public IActionResult SubPieceEditor(SubPieceModel model)
//        {
//            if (!ModelState.IsValid) return View(model);

//            // model.UpdatedBy = GetUserId(User.Identity);
//            var save = _pieceService.SaveSubPiece(model);
//            if (!save.Success)
//            {
//                ModelState.AddModelError(save.Data + string.Empty, save.Message);
//                return View(model);
//            }
//            AddActivityLog(this.HttpContext, Constants.LogType.Update, Constants.ResourceType.Admin, save.Message);
//            return View(model);
//        }
//        public IActionResult DeleteSubPiece(int id)
//        {
//            var result = _pieceService.DeleteSubPiece(id);
//            AddActivityLog(this.HttpContext, Constants.LogType.Delete, Constants.ResourceType.Admin, result.Message);

//            return Content("OK");
//        }
//    }
//}