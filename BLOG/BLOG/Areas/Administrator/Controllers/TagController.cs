﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using BLOG.App_Core.Attributes;
using BLOG.App_Core.Extensions;
using BLOG.Business.Posts;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Posts;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    [UserAuthorize]
    public class TagController : BaseController
    {
        private readonly ITagService _tagService;
        TagViewModel models = new TagViewModel();
        public TagController(ITagService tagService)
        {
            _tagService = tagService;
        }

        #region #Index 
        public IActionResult Index(string sortBy, string orderBy,string keyword, int pageIndex = 1, int pageSize = 10)
        {
            GlobalParamFilter filters = new GlobalParamFilter
            {
                OrderBy = orderBy,
                SortBy = sortBy,
                PageIndex = pageIndex,
                PageSize = pageSize > 0 ? pageSize : int.MaxValue,
                Keyword=keyword
            };
            var category = _tagService.GetAll(filters);
            if (category.Any())
            {
                models.ListTags= category.Select(c => c.MapTo<Tag, TagModel>()).ToList();          
            }
            models.keyWord = keyword;
            models.PageIndex = pageIndex;
            models.PageSize = pageSize;
            models.TotalPages = category.TotalPages;
            models.TotalCount = category.TotalCount;

            return View(models);
        }
        #endregion

        #region #Add
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(TagViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                model.InfoTag = model.InfoTag.TrimStringProperties();
                model.InfoTag.CreatedBy = GetUserId(User.Identity);
                var data = model.InfoTag.MapTo<TagModel, Tag>();
                data.CreateDate = DateTime.Now;
                _tagService.Add(data);
                _tagService.Save();

                AddActivityLog(this.HttpContext, Constants.LogType.Insert, Constants.ResourceType.Admin,
                    string.Format(Constants.MessageLog.Insert, "Tag", SerializeTool.Serialize(model.InfoTag.MapTo<TagModel, Tag>())));
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                FileUtils.LogFile(e);
                return RedirectToAction("Add");
            }
        }
        #endregion
        #region #Edit
        public IActionResult Edit(int id)
        {
            try {
                var data = _tagService.FirstOrDefault(c => c.Id == id);
                models.InfoTag = data.MapTo<Tag, TagModel>();
                return View(models);
            }
            catch(Exception e)
            {
                FileUtils.LogFile(e);
                return RedirectToAction("Index");
            }
           

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(TagViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    TempData[Constants.TempDataKey.Error] = Constants.Message.ValidateFail;
                    return View(model);
                }
                model.InfoTag = model.InfoTag.TrimStringProperties();
                var tag = _tagService.FirstOrDefault(c => c.Id == model.InfoTag.Id);

                tag.Name = model.InfoTag.Name;

                tag.UpdateDate = DateTime.Now;
                tag.UpdatedBy = GetUserId(User.Identity);
                _tagService.Edit(tag);
                _tagService.Save();
                TempData[Constants.TempDataKey.Success] = string.Format(Constants.Message.SuccessToUpdate, "Tag");
                //Log
                AddActivityLog(this.HttpContext, Constants.LogType.Update, Constants.ResourceType.Admin,
                    string.Format(Constants.MessageLog.Update, "Tag", SerializeTool.Serialize(tag)));
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                FileUtils.LogFile(e);
                TempData[Constants.TempDataKey.Error] = string.Format(Constants.Message.FailToUpdate, "Tag");
                return RedirectToAction("Edit");
            }
        }
        #endregion
        #region #Delete
        public IActionResult Delete(int id)
        {
            try
            {
                var userObj = _tagService.FirstOrDefault(c => c.Id == id);
                string logContent = string.Format(Constants.MessageLog.Delete, "Tag", SerializeTool.Serialize(userObj));
                _tagService.Delete(_tagService.Find(id));
                _tagService.Save();
                //Log
                AddActivityLog(this.HttpContext, Constants.LogType.Delete, Constants.ResourceType.Admin, logContent);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                FileUtils.LogFile(e);
                return RedirectToAction("Index");
            }
        }
        public IActionResult DeleteMany(int?[] ids)
        {
            try
            {
                if (ids.Any())
                {
                    var cate = _tagService.FindAllBy(c => ids.Contains(c.Id));
                    if (cate.Any())
                    {
                        _tagService.Delete(cate);
                        _tagService.Save();
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                FileUtils.LogFile(e);
                return RedirectToAction("Index");
            }

        }
        #endregion
        [HttpPost]
        public IActionResult CheckNameCreate(string name,int id=0)
        {
            if(id != 0)
            {
                if (_tagService.FindAllBy(c => c.Name == name && c.Id != id).Count() > 0)
                    return Json(false);
                return Json(true);
            }
            else
            {
                if (_tagService.FindAllBy(c => c.Name == name).Count() > 0)
                {
                    return Json(false);
                }
                else return Json(true);

            }

        }
       
        public IActionResult OpenTagInfo(int id)
        {
            var data = _tagService.FirstOrDefault(c => c.Id == id);
            var tagModel = data.MapTo<Tag, TagModel>();
            if (tagModel == null) tagModel = new TagModel();
            return View(tagModel);
        }

        [HttpPost]
        public IActionResult OpenTagInfo(TagModel model)
        {
            if (ModelState.IsValid)
            {
                model.UpdatedBy = GetUserId(User.Identity);
                var save = _tagService.SaveTag(model);
                if(!save.Success)
                {
                    ModelState.AddModelError(save.Data + string.Empty, save.Message);
                    return View(model);
                }
                TempData[Constants.TempDataKey.Success] = save.Message;
                AddActivityLog(this.HttpContext, (model.Id == 0) ? Constants.LogType.Insert : Constants.LogType.Update, Constants.ResourceType.Admin, save.Message);
                return View(model);
            }

            return View(model);
        }

      
        public IActionResult TagEditor(int id)
        {
            var data = _tagService.FirstOrDefault(c => c.Id == id);
            var tagModel = data.MapTo<Tag, TagModel>();
            if (tagModel == null) tagModel = new TagModel();
            return View(tagModel);
        }
    }
}