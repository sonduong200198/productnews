﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.IO;
using System.Linq;
using BLOG.App_Core.Attributes;
using BLOG.App_Core.Extensions;
using BLOG.Business.Categories;
using BLOG.Business.Posts;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Posts;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    [UserAuthorize]
    public class PostController : BaseController
    {
        private readonly IPostService _postService;
        private readonly ICategoryService _categoryService;
        private readonly ITagService _tagService;
     
        private readonly IHostingEnvironment hostingEnviroment;
        PostViewModel models = new PostViewModel();
        public PostController(IPostService postService, ICategoryService categoryService, ITagService tagService, IHostingEnvironment hostingEnviroment)
        {
            _tagService = tagService;
            _categoryService = categoryService;
            _postService = postService;
         
            this.hostingEnviroment = hostingEnviroment;
        }

        #region #Index
        public IActionResult Index(string sortBy, string orderBy,string keyword,int pageIndex = 1, int pageSize = 10)
        {
            keyword = (keyword + string.Empty).Replace("-", " ");
            GlobalParamFilter filters = new GlobalParamFilter
            {
                OrderBy = orderBy,
                SortBy = sortBy,
                PageIndex = pageIndex,
                PageSize = pageSize > 0 ? pageSize : int.MaxValue,
                Keyword = keyword
            };
            var posts = _postService.GetAll(filters);
            if (posts.Any())
            {

                models.ListPosts = posts.Select(c => c.MapTo<Post,PostModel>()).ToList();
                foreach (var item in models.ListPosts)
                {
                    var selected = _categoryService.FirstOrDefault(c => c.Id == item.CateId);
                    if (selected != null)
                    {
                        item.NameCate = selected.Title;
                    }
                }
            }
            models.keyWord = keyword;
            models.PageIndex = pageIndex;
            models.PageSize = pageSize;
            models.TotalPages = posts.TotalPages;
            models.TotalCount = posts.TotalCount;

            return View(models);
        }
        #endregion
        #region #Add
        public IActionResult Add()
        {
            try
            {
                var data = _categoryService.FindAllBy(c=>c.Type ==Constants.CategoryType.Blog || c.Type == Constants.CategoryType.General).ToList();
                if (data.Any() && data != null)
                {
                    models.Cates.AddRange(data.Select(c => new SelectListItem()
                    {
                        Text = c.Title,
                        Value = c.Id.ToString()
                    }));
                }
                var ltag = _tagService.GetAll().ToList();
                if (ltag.Any() && ltag != null)
                {
                    models.ListTags.AddRange(ltag.Select(c => new SelectListItem()
                    {
                        Text = c.Name,
                        Value = c.Id.ToString()
                    }));
                }
              
                return View(models);
            }
            catch(Exception e)
            {
                FileUtils.LogFile(e);
                return RedirectToAction("Index");
            } 
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(PostViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                string uniqueFileName = null;
                if (model.InfoPost.Photo != null && model.InfoPost.Photo.Length != 0)
                {
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + model.InfoPost.Photo.FileName;
                        string uploadsForder = Path.Combine(hostingEnviroment.WebRootPath, "images");
                        string filePath = Path.Combine(uploadsForder, uniqueFileName);
                        model.InfoPost.Photo.CopyTo(new FileStream(filePath, FileMode.Create));
                }
                
                model.InfoPost.Image = "/images/" + uniqueFileName;
                if(model.InfoPost.ListTag != null)
                {
                    model.InfoPost.Tags = string.Join(",", model.InfoPost.ListTag);
                }
                if(model.InfoPost.ListFaceId != null)
                {
                    model.InfoPost.FaceIds = string.Join(",", model.InfoPost.ListFaceId);
                }
                model.InfoPost = model.InfoPost.TrimStringProperties();
                model.InfoPost.CreatedBy = GetUserId(User.Identity);
                var data = model.InfoPost.MapTo<PostModel, Post>();
                data.CreateDate = DateTime.Now;
                _postService.Add(data);
                _postService.Save();

                AddActivityLog(this.HttpContext, Constants.LogType.Insert, Constants.ResourceType.Admin,
                    string.Format(Constants.MessageLog.Insert, "Post", SerializeTool.Serialize(model.InfoPost.MapTo<PostModel, Post>())));
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                FileUtils.LogFile(e);
                return RedirectToAction("Add");
            }
        }
        #endregion
        #region #Edit
        public IActionResult Edit(int id)
        {
            try
            {
                var data = _categoryService.FindAllBy(c=>c.Type == Constants.CategoryType.General || c.Type == Constants.CategoryType.Blog).ToList();
                if (data.Any() && data != null)
                {
                    models.Cates.AddRange(data.Select(c => new SelectListItem()
                    {
                        Text = c.KeyName,
                        Value = c.Id.ToString()
                    }));
                }
                var ltag = _tagService.GetAll().ToList();
                if (ltag.Any() && ltag != null)
                {
                    models.ListTags.AddRange(ltag.Select(c => new SelectListItem()
                    {
                        Text = c.Name,
                        Value = c.Id.ToString()
                    }));
                }
                models.InfoPost = _postService.FirstOrDefault(c => c.Id == id).MapTo<Post, PostModel>();
                if (!String.IsNullOrEmpty(models.InfoPost.Tags))
                {
                    models.InfoPost.ListTag = models.InfoPost.Tags.Split(",".ToCharArray());
                }
                
                if (!String.IsNullOrEmpty(models.InfoPost.FaceIds))
                {
                    models.InfoPost.ListFaceId = models.InfoPost.FaceIds.Split(",".ToCharArray());
                }
                return View(models);
            }
            catch(Exception e)
            {
                FileUtils.LogFile(e);
                return RedirectToAction("Index");
            }
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(PostViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    TempData[Constants.TempDataKey.Error] = Constants.Message.ValidateFail;
                    return View(model);
                }
                model.InfoPost = model.InfoPost.TrimStringProperties();
                var post = _postService.FirstOrDefault(c => c.Id == model.InfoPost.Id);
                //Save Image
                string uniqueFileName = null;
                if (model.InfoPost.Photo != null && model.InfoPost.Photo.Length != 0)
                {
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + model.InfoPost.Photo.FileName;
                    string name= "/images/" + uniqueFileName;
                    if (name != post.Image)
                    {
                        string uploadsForder = Path.Combine(hostingEnviroment.WebRootPath, "images");
                        string filePath = Path.Combine(uploadsForder, uniqueFileName);
                        model.InfoPost.Photo.CopyTo(new FileStream(filePath, FileMode.Create));
                        post.Image = "/images/" + uniqueFileName;
                    }
                } 
                post.Name = model.InfoPost.Name;
                post.Title = model.InfoPost.Title;
                post.SlugUrl = model.InfoPost.SlugUrl;
                post.Contents = model.InfoPost.Contents;
                post.Description = model.InfoPost.Description;
                post.Keywords = model.InfoPost.Keywords;
                post.Type = model.InfoPost.Type;
                post.CateId = model.InfoPost.CateId;
                if(model.InfoPost.ListTag != null)
                {
                    post.Tags = string.Join(",", model.InfoPost.ListTag);
                }
                if (model.InfoPost.ListFaceId != null)
                {
                    post.FaceIds = string.Join(",", model.InfoPost.ListFaceId);
                }
                post.Status = model.InfoPost.Status;
                post.UpdateDate = DateTime.Now;
                post.UpdatedBy = GetUserId(User.Identity);
                _postService.Edit(post);
                _postService.Save();
                TempData[Constants.TempDataKey.Success] = string.Format(Constants.Message.SuccessToUpdate, "Category");
                //Log
                AddActivityLog(this.HttpContext, Constants.LogType.Update, Constants.ResourceType.Admin,
                    string.Format(Constants.MessageLog.Update, "Category", SerializeTool.Serialize(post)));
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                FileUtils.LogFile(e);
                TempData[Constants.TempDataKey.Error] = string.Format(Constants.Message.FailToUpdate, "Category");
                return RedirectToAction("Edit");
            }
        }
        #endregion
        #region #Delete
        public IActionResult Delete(int id)
        {
            try
            {
                var userObj = _postService.FirstOrDefault(c => c.Id == id);
                string logContent = string.Format(Constants.MessageLog.Delete, "Post", SerializeTool.Serialize(userObj));
                _postService.Delete(_postService.Find(id));
               
                _postService.Save();
                AddActivityLog(this.HttpContext, Constants.LogType.Delete, Constants.ResourceType.Admin, logContent);
                return Json(true);
            }
            catch (Exception e)
            {
                FileUtils.LogFile(e);
                return Json(false);
            }
        }
        public IActionResult DeleteMany(int?[] ids)
        {
            try
            {
                if (ids.Any())
                {
                    var posts = _postService.FindAllBy(c => ids.Contains(c.Id));
                    if (posts.Any())
                    {
                        _postService.Delete(posts);
                        _postService.Save();
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                FileUtils.LogFile(e);
                return RedirectToAction("Index");
            }

        }
        #endregion
    }
}