﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BLOG.App_Core.Attributes;
using BLOG.App_Core.Extensions;
using BLOG.Areas.Administrator.Models.Systems;
using BLOG.Business.Sys;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Sys;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    [UserAuthorize]
    public class MenuGroupsController : BaseController
    {
        MenuGroupViewModel model = new MenuGroupViewModel();
        private readonly ISysMenuGroupService _menuGroupService;
        public MenuGroupsController(ISysMenuGroupService menuGroupService)
        {
            _menuGroupService = menuGroupService;
        }

        #region #Index

        public IActionResult Index()
        {
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> GetList(string keyword, string sortBy, string orderBy, int pageIndex = 1, int pageSize = 10)
        {
            GlobalParamFilter filters = new GlobalParamFilter
            {
                Keyword = string.IsNullOrEmpty(keyword) ? string.Empty : keyword.Trim(),
                OrderBy = orderBy,
                SortBy = sortBy,
                PageIndex = pageIndex,
                PageSize = pageSize > 0 ? pageSize : int.MaxValue
            };
            var menuGroups = await _menuGroupService.GetAll(filters);

            if (menuGroups.Any())
            {
                model.MenuGroupItems = menuGroups.MapTo(new List<SysMenuGroupModel>()).ToList();
            }
            model.PageIndex = pageIndex;
            model.PageSize = pageSize;
            model.TotalPages = menuGroups.TotalPages;
            model.TotalCount = menuGroups.TotalCount;
            return Json(model);
        }
        #endregion

        #region #Insert, update data

        [HttpPost]
        public IActionResult UpdateData(SysMenuGroupModel item)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.Error = true;
                    model.Message = Constants.Message.ValidateFail;
                    return Json(model);
                }
                item = item.TrimStringProperties();
                if (item.Id == 0)
                {
                    //Insert
                    item.CreatedBy = GetUserId(User.Identity);
                    _menuGroupService.Add(item.MapTo<SysMenuGroupModel, SysMenuGroup>());
                    _menuGroupService.Save();
                    model.Error = false;
                    model.Message = string.Format(Constants.Message.SuccessToCreate,"Menu group");
                    //Log
                    AddActivityLog(this.HttpContext, Constants.LogType.Insert, Constants.ResourceType.Admin,
                        string.Format(Constants.MessageLog.Insert, "Menu group", SerializeTool.Serialize(item.MapTo<SysMenuGroupModel, SysMenuGroup>())));
                }
                else
                {
                    //Update
                    var menuGroupObj = _menuGroupService.FirstOrDefault(m => m.Id == item.Id);
                    if (menuGroupObj != null)
                    {
                        menuGroupObj.Name = item.Name;
                        menuGroupObj.Icon = item.Icon;
                        menuGroupObj.Description = item.Description;
                        menuGroupObj.UpdatedBy = GetUserId(User.Identity);
                        menuGroupObj.UpdatedDate = DateTime.Now;
                        _menuGroupService.Edit(menuGroupObj);
                        _menuGroupService.Save();

                        model.Error = false;
                        model.Message = string.Format(Constants.Message.SuccessToUpdate, "Menu group");

                        //Log
                        AddActivityLog(this.HttpContext, Constants.LogType.Update, Constants.ResourceType.Admin,
                            string.Format(Constants.MessageLog.Update, "Menu group", SerializeTool.Serialize(menuGroupObj)));
                    }
                }
            }
            catch
            {
                model.Error = true;
                model.Message = Constants.Message.ApplicationError;
            }

            return Json(model);
        }

        #endregion

        #region #Delete
        [HttpPost]
        public IActionResult Delete(int id)
        {
            try
            {
                var menuGroupObj = _menuGroupService.FirstOrDefault(m => m.Id == id);
                if (menuGroupObj == null)
                {
                    model.Error = true;
                    model.Message = string.Format(Constants.Message.IsNotExists, "Menu group");
                }
                else
                {
                    string logContent = string.Format(Constants.MessageLog.Delete, "Menu group", SerializeTool.Serialize(menuGroupObj));
                    _menuGroupService.Delete(menuGroupObj);
                    _menuGroupService.Save();

                    model.Error = false;
                    model.Message = string.Format(Constants.Message.SuccessToDelete, "Menu group");
                    //Log
                    AddActivityLog(this.HttpContext, Constants.LogType.Delete, Constants.ResourceType.Admin, logContent);
                }
            }
            catch
            {
                model.Error = true;
                model.Message = Constants.Message.ApplicationError;
            }
            return Json(model);
        }

        #endregion
    }
}