﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BLOG.Business.Users;
using BLOG.Core.Constants;
using BLOG.Core.Utility;
using BLOG.Models.Users;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    public class LoginController : Controller
    {
        private readonly IUserService _userService;
        public LoginController(IUserService userService) { 
            _userService = userService;
        }

        public IActionResult Index(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult Index(LoginModel model, string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (!ModelState.IsValid) return View(model);

            // 1.Check user exists
            var user = _userService.FirstOrDefault(m => m.UserName == model.Username && m.Status == Constants.Status.Active);
            if (user == null)
            {
                ModelState.AddModelError("Username", @"The user or password is incorrect");
                return View(model);
            }

            // 2.Check password of the user
            string password = string.Concat(model.Password, user.SaltKey);
            if (!string.Equals(user.Password, StringTools.Encryption(password)))
            {
                ModelState.AddModelError("Username", @"The user or password is incorrect");
                return View(model);
            }

            //Create the identity for the user  
            var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, model.Username), new Claim(ClaimTypes.Sid, user.Id.ToString()) }, CookieAuthenticationDefaults.AuthenticationScheme);

            var principal = new ClaimsPrincipal(identity);

            HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
            return !string.IsNullOrEmpty(returnUrl) ? Redirect(returnUrl) : Redirect("/Administrator");
        }

        public IActionResult SignOut()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index");
        }
    }
}