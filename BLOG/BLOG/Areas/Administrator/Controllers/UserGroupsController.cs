﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BLOG.App_Core.Attributes;
using BLOG.App_Core.Extensions;
using BLOG.Areas.Administrator.Models.Users;
using BLOG.Business.Sys;
using BLOG.Business.Users;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Sys;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    [UserAuthorize]
    public class UserGroupsController : BaseController
    {
        UserGroupViewModel model = new UserGroupViewModel();
        private readonly ISysGroupFunctionService _groupFunctionService;
        private readonly IUserGroupFunctionsService _userGroupFunctionsService;

        public UserGroupsController(ISysGroupFunctionService groupFunctionService, IUserGroupFunctionsService userGroupFunctionsService)
        {
            _groupFunctionService = groupFunctionService;
            _userGroupFunctionsService = userGroupFunctionsService;
        }

        #region #Index

        public IActionResult Index()
        {
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> GetList(string keyword, string sortBy, string orderBy, int pageIndex = 1, int pageSize = 10)
        {
            GlobalParamFilter filters = new GlobalParamFilter
            {
                Keyword = string.IsNullOrEmpty(keyword) ? string.Empty : keyword.Trim(),
                OrderBy = orderBy,
                SortBy = sortBy,
                PageIndex = pageIndex,
                PageSize = pageSize > 0 ? pageSize : int.MaxValue
            };
            var groupFunctions = await _groupFunctionService.GetAll(filters);

            if (groupFunctions.Any())
            {
                model.GroupFunctionItems = groupFunctions.MapTo(new List<SysGroupFunctionModel>()).ToList();
                foreach (var item in model.GroupFunctionItems)
                {
                    item.TotalUsers = _userGroupFunctionsService.Count(m => m.GroupFunctionId == item.Id);
                }
            }
            model.PageIndex = pageIndex;
            model.PageSize = pageSize;
            model.TotalPages = groupFunctions.TotalPages;
            model.TotalCount = groupFunctions.TotalCount;
            return Json(model);
        }
        #endregion

        #region #Insert, update data

        [HttpPost]
        public IActionResult UpdateData(SysGroupFunctionModel item)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.Error = true;
                    model.Message = Constants.Message.ValidateFail;
                    return Json(model);
                }
                item = item.TrimStringProperties();
                if (item.Id == 0)
                {
                    //Insert
                    item.CreatedBy = GetUserId(User.Identity);
                    _groupFunctionService.Add(item.MapTo<SysGroupFunctionModel, SysGroupFunction>());
                    _groupFunctionService.Save();
                    model.Error = false;
                    model.Message = string.Format(Constants.Message.SuccessToCreate, "User Group");
                    //Log
                    AddActivityLog(this.HttpContext, Constants.LogType.Insert, Constants.ResourceType.Admin,
                        string.Format(Constants.MessageLog.Insert, "User Group", SerializeTool.Serialize(item.MapTo<SysGroupFunctionModel, SysGroupFunction>())));
                }
                else
                {
                    //Update
                    var menuGroupObj = _groupFunctionService.FirstOrDefault(m => m.Id == item.Id);
                    if (menuGroupObj != null)
                    {
                        menuGroupObj.Name = item.Name;
                        menuGroupObj.Description = item.Description;
                        menuGroupObj.UpdatedBy = GetUserId(User.Identity);
                        menuGroupObj.UpdatedDate = DateTime.Now;
                        _groupFunctionService.Edit(menuGroupObj);
                        _groupFunctionService.Save();

                        model.Error = false;
                        model.Message = string.Format(Constants.Message.SuccessToUpdate, "User Group");

                        //Log
                        AddActivityLog(this.HttpContext, Constants.LogType.Update, Constants.ResourceType.Admin,
                            string.Format(Constants.MessageLog.Update, "User Group", SerializeTool.Serialize(menuGroupObj)));
                    }
                }
            }
            catch
            {
                model.Error = true;
                model.Message = Constants.Message.ApplicationError;
            }

            return Json(model);
        }

        #endregion

        #region #Delete
        [HttpPost]
        public IActionResult Delete(int id)
        {
            try
            {
                var groupFunctionObj = _groupFunctionService.FirstOrDefault(m => m.Id == id);
                if (groupFunctionObj == null)
                {
                    model.Error = true;
                    model.Message = string.Format(Constants.Message.IsNotExists, "User Group");
                }
                else
                {
                    string logContent = string.Format(Constants.MessageLog.Delete, "User Group", SerializeTool.Serialize(groupFunctionObj));
                    bool result = _groupFunctionService.Delete(id);
                    if (result)
                    {
                        model.Error = false;
                        model.Message = string.Format(Constants.Message.SuccessToDelete, "User Group");
                        //Log
                        AddActivityLog(this.HttpContext, Constants.LogType.Delete, Constants.ResourceType.Admin, logContent);
                    }
                    else
                    {
                        model.Error = true;
                        model.Message = Constants.Message.ApplicationError;
                    }
                }
            }
            catch
            {
                model.Error = true;
                model.Message = Constants.Message.ApplicationError;
            }
            return Json(model);
        }

        #endregion

    }
}