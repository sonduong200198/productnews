﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using BLOG.Business.Sys;
using BLOG.Business.Users;
using BLOG.Core.Constants;
using BLOG.Entities.Entities;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    public class BaseController : Controller
    {
        public int GetUserId(IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
            Claim claim = claimsIdentity?.FindFirst(ClaimTypes.Sid);

            if (claim == null)
                return 0;

            return int.Parse(claim.Value);
        }

        #region #Options
        public List<SelectListItem> GetStatusOptions(bool all)
        {
            List<SelectListItem> opt = new List<SelectListItem>();
            if (all)
                opt.Add(new SelectListItem("Select all--", string.Empty));
            opt.Add(new SelectListItem("Publish", Constants.Status.Publish));
            opt.Add(new SelectListItem("Draft", Constants.Status.Draft));
            opt.Add(new SelectListItem("Delete", Constants.Status.Delete));
            return opt;
        }

        public List<SelectListItem> GetLogTypeOptions(bool all)
        {
            List<SelectListItem> opt = new List<SelectListItem>();
            if (all)
                opt.Add(new SelectListItem("Select action--", "ALL"));
            opt.Add(new SelectListItem { Text = "Action Insert", Value = Constants.LogType.Insert });
            opt.Add(new SelectListItem { Text = "Action Update", Value = Constants.LogType.Update });
            opt.Add(new SelectListItem { Text = "Action Delete", Value = Constants.LogType.Delete });
            opt.Add(new SelectListItem { Text = "Action Search", Value = Constants.LogType.Search });
            return opt;
        }
        #endregion

        #region #Log
        /// <summary>
        /// Save log action : add, edit, delete, search
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="activityLogTypeName">Insert, Update, Delete,...</param>
        /// <param name="resourceType">ADMIN, End user ...</param>
        /// <param name="comment"></param>
        public void AddActivityLog(HttpContext httpContext, string activityLogTypeName, string resourceType, string comment)
        {
            try
            {
                var userService = (IUserService)httpContext.RequestServices.GetService(typeof(IUserService));
                var logService = (ISysActivityLogService)httpContext.RequestServices.GetService(typeof(ISysActivityLogService));
                var userObj = userService.FirstOrDefault(m => m.UserName == User.Identity.Name);
                if (userObj != null)
                {
                    SysActivityLog activityLog = new SysActivityLog
                    {
                        LogType = activityLogTypeName,
                        SessionId = Guid.NewGuid().ToString(),
                        Comment = comment,
                        ResourceType = resourceType,
                        UserId = userObj.Id,
                        CreateDate = DateTime.Now
                    };
                    logService.Add(activityLog);
                    logService.Save();
                }
            }
            catch
            {
                //
            }

        }

        #endregion
    }
}