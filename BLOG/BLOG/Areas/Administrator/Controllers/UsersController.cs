﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using BLOG.App_Core.Attributes;
using BLOG.App_Core.Extensions;
using BLOG.Areas.Administrator.Models.Users;
using BLOG.Business.Sys;
using BLOG.Business.Users;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Sys;
using BLOG.Models.Users;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    [UserAuthorize]
    public class UsersController : BaseController
    {
        UserViewModel model = new UserViewModel();
        private readonly IUserService _userService;
        private readonly ISysGroupFunctionService _groupFunctionService;
        private readonly IUserGroupFunctionsService _userGroupFunctionsService;

        public UsersController(IUserService userService, ISysGroupFunctionService groupFunctionService,
            IUserGroupFunctionsService userGroupFunctionsService)
        {
            _userService = userService;
            _groupFunctionService = groupFunctionService;
            _userGroupFunctionsService = userGroupFunctionsService;
        }

        #region #Index

        public IActionResult Index()
        {
            model.StatusOptions = GetStatusOptions(true);
            model.GroupOptions = GetGroupOptions(true);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> GetList(string keyword, int groupId, int? status, string sortBy, string orderBy, int pageIndex = 1, int pageSize = 10)
        {
            GlobalParamFilter filters = new GlobalParamFilter
            {
                Status = status,
                Keyword = string.IsNullOrEmpty(keyword) ? string.Empty : keyword.Trim(),
                GroupId = groupId,
                OrderBy = orderBy,
                SortBy = sortBy,
                PageIndex = pageIndex,
                PageSize = pageSize > 0 ? pageSize : int.MaxValue
            };
            var users = await _userService.GetAll(filters);

            if (users.Any())
            {
                model.UserItems = users.Select(c => c.MapTo<User, UserModel>()).ToList();
                foreach (var item in model.UserItems)
                {
                    item.SaltKey = string.Empty;
                    item.Password = string.Empty;
                    item.GroupFunctionItems = _groupFunctionService.GetByUser(item.Id).MapTo(new List<SysGroupFunctionModel>()).ToList();
                }
            }
            model.PageIndex = pageIndex;
            model.PageSize = pageSize;
            model.TotalPages = users.TotalPages;
            model.TotalCount = users.TotalCount;
            return Json(model);
        }
        #endregion

        #region #Add

        public IActionResult Add()
        {
            model.StatusOptions = GetStatusOptions(false);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(UserModel item)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.UserInfo = item;
                    model.StatusOptions = GetStatusOptions(false);
                    TempData[Constants.TempDataKey.Error] = Constants.Message.ValidateFail;
                    return View(model);
                }

                var userObj = _userService.FirstOrDefault(m => m.UserName == item.UserName);
                if (userObj != null)
                {
                    model.UserInfo = item;
                    model.StatusOptions = GetStatusOptions(false);
                    TempData[Constants.TempDataKey.Error] = string.Format(Constants.Message.IsExists, "User");
                    return View(model);
                }

                item.SaltKey = StringTools.CreateSaltKey(8);
                item.Password = StringTools.Encryption(string.Concat(item.Password, item.SaltKey));
                item.CreatedBy = GetUserId(User.Identity);
                item = item.TrimStringProperties();
                _userService.Add(item.MapTo<UserModel, User>());
                _userService.Save();
                TempData[Constants.TempDataKey.Success] = string.Format(Constants.Message.SuccessToCreate, "User");
                //Log
                AddActivityLog(this.HttpContext, Constants.LogType.Insert, Constants.ResourceType.Admin,
                    string.Format(Constants.MessageLog.Insert, "User", SerializeTool.Serialize(item.MapTo<UserModel, User>())));
                return RedirectToAction("Index");
            }
            catch
            {
                TempData[Constants.TempDataKey.Error] = string.Format(Constants.Message.FailToCreate, "User");
                return RedirectToAction("Add");
            }
        }

        #endregion

        #region #Edit

        public IActionResult Edit(int id)
        {
            var userObj = _userService.FirstOrDefault(m => m.Id == id);
            if (userObj == null)
                return RedirectToAction("Index");
            model.UserInfo = userObj.MapTo<User, UserModel>();
            model.StatusOptions = GetStatusOptions(false);
            return View(model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, UserModel item)
        {
            try
            {
                var userObj = _userService.FirstOrDefault(m => m.Id == id);
                if (userObj == null)
                    return RedirectToAction("Index");

                if (!ModelState.IsValid)
                {
                    model.UserInfo = item;
                    model.StatusOptions = GetStatusOptions(false);
                    TempData[Constants.TempDataKey.Error] = Constants.Message.ValidateFail;
                    return View(model);
                }
                var checkUserName = _userService.FirstOrDefault(m => m.UserName == item.UserName);
                if (checkUserName != null && checkUserName.Id != id)
                {
                    model.UserInfo = item;
                    model.StatusOptions = GetStatusOptions(false);
                    TempData[Constants.TempDataKey.Error] = string.Format(Constants.Message.IsExists, "Username");
                    return View(model);
                }
                item = item.TrimStringProperties();
                userObj.UserName = item.UserName;
                userObj.Email = item.Email;
                userObj.FirstName = item.FirstName;
                userObj.LastName = item.LastName;
                userObj.Status = item.Status;
                userObj.UpdatedDate = DateTime.Now;
                userObj.UpdatedBy = GetUserId(User.Identity);
                _userService.Edit(userObj);
                _userService.Save();
                TempData[Constants.TempDataKey.Success] = string.Format(Constants.Message.SuccessToUpdate, "User");
                //Log
                AddActivityLog(this.HttpContext, Constants.LogType.Update, Constants.ResourceType.Admin,
                    string.Format(Constants.MessageLog.Update, "User", SerializeTool.Serialize(userObj)));
                return RedirectToAction("Index");
            }
            catch
            {
                TempData[Constants.TempDataKey.Error] = string.Format(Constants.Message.FailToUpdate, "User");
                return RedirectToAction("Index");
            }
        }
        #endregion

        #region #Delete

        [HttpPost]
        public IActionResult Delete(int id)
        {
            try
            {
                var userObj = _userService.FirstOrDefault(m => m.Id == id);
                if (userObj == null)
                {
                    model.Error = true;
                    model.Message = string.Format(Constants.Message.IsNotExists, "User");
                }
                else
                {
                    string logContent = string.Format(Constants.MessageLog.Delete, "User", SerializeTool.Serialize(userObj));
                    bool result = _userService.Delete(id);
                    if (result)
                    {
                        model.Error = false;
                        model.Message = string.Format(Constants.Message.SuccessToDelete, "User");
                        //Log
                        AddActivityLog(this.HttpContext, Constants.LogType.Delete, Constants.ResourceType.Admin, logContent);
                    }
                    else
                    {
                        model.Error = true;
                        model.Message = Constants.Message.ApplicationError;
                    }
                }
            }
            catch
            {
                model.Error = true;
                model.Message = Constants.Message.ApplicationError;
            }
            return Json(model);
        }

        #endregion

        #region #Reset password

        [HttpPost]
        public IActionResult ResetPassword(int id)
        {
            try
            {
                var userObj = _userService.FirstOrDefault(m => m.Id == id);
                if (userObj == null)
                {
                    model.Error = true;
                    model.Message = string.Format(Constants.Message.IsNotExists, "User");
                }
                else
                {
                    string password = StringTools.RandomCode(8);
                    userObj.Password = StringTools.Encryption(string.Concat(password, userObj.SaltKey));
                    userObj.UpdatedBy = GetUserId(User.Identity);
                    userObj.UpdatedDate = DateTime.Now;
                    _userService.Edit(userObj);
                    _userService.Save();

                    //Log
                    string logContent = string.Format(Constants.MessageLog.Delete, "User", SerializeTool.Serialize(userObj));
                    AddActivityLog(this.HttpContext, Constants.LogType.Update, Constants.ResourceType.Admin, logContent);

                    model.Error = false;
                    model.Message = $"The new password is: {password}";
                }
            }
            catch
            {
                model.Error = true;
                model.Message = Constants.Message.ApplicationError;
            }

            return Json(model);
        }
        #endregion

        #region #Change password

        public IActionResult ChangePassword()
        {
            var modelView = new ResetPasswordViewModel();
            return View(modelView);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChangePassword(ResetPasswordViewModel item)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(item);
                var userObj = _userService.FirstOrDefault(m => m.UserName == User.Identity.Name);
                if (userObj == null)
                {
                    TempData[Constants.TempDataKey.Error] = string.Format(Constants.Message.IsNotExists, "User");
                    return View(item);
                }
                if (StringTools.Encryption(string.Concat(item.OldPassword, userObj.SaltKey)) != userObj.Password)
                {
                    ModelState.AddModelError("OldPassword", Constants.Message.PasswordIncorrect);
                    return View(item);
                }

                userObj.Password = StringTools.Encryption(string.Concat(item.Password, userObj.SaltKey));
                _userService.Edit(userObj);
                _userService.Save();
                TempData[Constants.TempDataKey.Success] = string.Format(Constants.Message.SuccessToUpdate, "Password");
            }
            catch
            {
                TempData[Constants.TempDataKey.Error] = string.Format(Constants.Message.FailToUpdate, "Password");
            }

            return RedirectToAction("ChangePassword");
        }
        #endregion

        #region #Validate
        /// <summary>
        /// Validate User exist
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ExistUserName(int id, string name)
        {
            var obj = _userService.FirstOrDefault(m => m.UserName == name);
            if (obj != null && obj.Id != id)
                return Json("existed");
            return Json(string.Empty);
        }

        #endregion

        #region #User group

        [HttpPost]
        public IActionResult GetRoleByUser(int id)
        {
            try
            {
                var userGroupFunctions = _userGroupFunctionsService.FindAllBy(m => m.UserId == id).Select(c => c.GroupFunctionId).ToList();
                var groupFunctions = _groupFunctionService.GetAll().ToList();
                if (groupFunctions.Any())
                {
                    model.GroupFunctionItems = groupFunctions.MapTo(new List<SysGroupFunctionModel>()).ToList();
                    foreach (var item in model.GroupFunctionItems)
                    {
                        item.IsChecked = userGroupFunctions.Contains(item.Id);
                    }
                }
            }
            catch
            {
                //
            }

            return Json(model);
        }

        /// <summary>
        /// save role by user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult SaveRole(int userId, int[] roleIds)
        {
            try
            {
                var userGroupFunctions = _userGroupFunctionsService.FindAllBy(m => m.UserId == userId).AsQueryable();
                if (userGroupFunctions.Any())
                {
                    _userGroupFunctionsService.Delete(userGroupFunctions);
                    _userGroupFunctionsService.Save();
                }

                var userGroups = new List<UserGroupFunction>();
                foreach (var item in roleIds)
                {
                    userGroups.Add(new UserGroupFunction { UserId = userId, GroupFunctionId = item });
                }

                if (userGroups.Any())
                {
                    _userGroupFunctionsService.Add(userGroups.AsQueryable());
                    _userGroupFunctionsService.Save();
                }
                model.Error = false;
                model.Message = string.Format(Constants.Message.SuccessToUpdate, "Assign user for group");
            }
            catch
            {
                model.Error = true;
                model.Message = Constants.Message.ApplicationError;
            }

            return Json(model);
        }

        #endregion

        #region #Options

        private List<SelectListItem> GetGroupOptions(bool all)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            if (all)
                result.Add(new SelectListItem() { Text = "Select all group", Value = "0" });
            var groups = _groupFunctionService.GetAll().ToList();
            foreach (var item in groups)
            {
                result.Add(new SelectListItem() { Text = item.Name, Value = item.Id.ToString() });
            }
            return result;
        }

        #endregion
    }
}