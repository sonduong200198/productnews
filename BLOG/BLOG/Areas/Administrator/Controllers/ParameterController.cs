﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using BLOG.App_Core.Attributes;
using BLOG.App_Core.Extensions;
using BLOG.Business.Parameters;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Core.Utility;
using BLOG.Entities.Entities;
using BLOG.Models.Parameter;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    [UserAuthorize]
    public class ParameterController : BaseController
    {
        private readonly IParameterService _parameterService;
        ParameterViewModel models = new ParameterViewModel();
        public ParameterController(IParameterService parameterService)
        {
            _parameterService = parameterService;
        }

        #region #Index
        public IActionResult Index(string sortBy, string orderBy, string keyword, int pageIndex = 1, int pageSize = 50, string type = "")
        {
            GlobalParamFilter filters = new GlobalParamFilter
            {
                OrderBy = orderBy,
                SortBy = sortBy,
                PageIndex = pageIndex,
                PageSize = pageSize > 0 ? pageSize : int.MaxValue,
                Keyword = keyword,
                Type = type
            };
            var category = _parameterService.GetAll(filters);
            if (category.Any())
            {
                models.ListParameters = category.Select(c => c.MapTo<Parameter, ParameterModel>()).ToList();
               
            }
            models.keyWord = keyword;
            models.PageIndex = pageIndex;
            models.PageSize = pageSize;
            models.TotalPages = category.TotalPages;
            models.TotalCount = category.TotalCount;

            return View(models);
        }
        #endregion
        public IActionResult OpenParameterInfo(int id)
        {
            var data = _parameterService.FirstOrDefault(c => c.Id == id);
            var subModel = data.MapTo<Parameter, ParameterModel>();
            if (subModel == null) subModel = new ParameterModel();
            return View(subModel);
        }

        [HttpPost]
        public IActionResult OpenParameterInfo(ParameterModel model)
        {
            if (ModelState.IsValid)
            {
                model.UpdatedBy = GetUserId(User.Identity);
                var save = _parameterService.SaveParameter(model);
                if (!save.Success)
                {
                    ModelState.AddModelError(save.Data + string.Empty, save.Message);
                    return View(model);
                }
                TempData[Constants.TempDataKey.Success] = save.Message;
                AddActivityLog(this.HttpContext, (model.Id == 0) ? Constants.LogType.Insert : Constants.LogType.Update, Constants.ResourceType.Admin, save.Message);
                return View(model);
            }
            return View(model);
        }
        #region #Delete
        public IActionResult Delete(int id)
        {
            try
            {
                var userObj = _parameterService.FirstOrDefault(c => c.Id == id);
                string logContent = string.Format(Constants.MessageLog.Delete, "Category", SerializeTool.Serialize(userObj));
                _parameterService.Delete(userObj);

                _parameterService.Save();
                AddActivityLog(this.HttpContext, Constants.LogType.Delete, Constants.ResourceType.Admin, logContent);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                FileUtils.LogFile(e);
                return RedirectToAction("Index");
            }
        }
        #endregion
    }
}