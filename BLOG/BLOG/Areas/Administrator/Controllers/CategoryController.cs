﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using BLOG.App_Core.Attributes;
using BLOG.App_Core.Extensions;
using BLOG.Core.Filters;
using BLOG.Core.Utility;
using BLOG.Models.Categories;
using Constants = BLOG.Core.Constants.Constants;
using BLOG.Business.Categories;
using BLOG.Business.Parameters;
using BLOG.Entities.Entities;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    [UserAuthorize]
    public class CategoryController : BaseController
    {

        private readonly ICategoryService _categoryService;
        private readonly IParameterService _parameterService;

        CategoryViewModel models = new CategoryViewModel();
        public CategoryController(ICategoryService categoryService, IParameterService parameterService)
        {
            _categoryService = categoryService;
            _parameterService = parameterService;
        }

        #region #Index
        public IActionResult Index(string sortBy, string orderBy, string keyword, int pageIndex = 1, int pageSize = 20, string type = "")
        {
            GlobalParamFilter filters = new GlobalParamFilter
            {
                OrderBy = orderBy,
                SortBy = sortBy,
                PageIndex = pageIndex,
                PageSize = pageSize > 0 ? pageSize : int.MaxValue,
                Keyword = keyword,       
                Type = type
            };
            var category = _categoryService.GetAll(filters);
            if (category.Any())
            {
                models.ListCategory = category.Select(c => c.MapTo<Category, CategoryModel>()).ToList();
                foreach (var item in models.ListCategory)
                {
                    var selected = category.FirstOrDefault(c => c.Id == item.ParentId);
                    if (selected != null)
                    {
                        item.NameParent = selected.Name;
                    }

                }
            }
            models.keyWord = keyword;
            models.PageIndex = pageIndex;
            models.PageSize = pageSize;
            models.TotalPages = category.TotalPages;
            models.TotalCount = category.TotalCount;

            return View(models);
        }

        #endregion

        //#endregion
        #region #Add
        public IActionResult Add()
        {
            var data = _categoryService.GetAll().ToList();
            if (data.Any() && data != null)
            {
                models.CategoriesParent.AddRange(data.Select(c => new SelectListItem()
                {
                    Text = c.Name,
                    Value = c.Id.ToString()
                }));
            }
            if (models.CategoriesParent.Any())
            {
                return View(models);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(CategoryViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                model.InfoCategory = model.InfoCategory.TrimStringProperties();
                model.InfoCategory.CreatedBy = GetUserId(User.Identity);
                var data = model.InfoCategory.MapTo<CategoryModel, Category>();
                data.CreateDate = DateTime.Now;
                _categoryService.Add(data);
                _categoryService.Save();
                TempData[Constants.TempDataKey.Success] = string.Format(Constants.Message.SuccessToUpdate, "Category");
                AddActivityLog(this.HttpContext, Constants.LogType.Insert, Constants.ResourceType.Admin,
                    string.Format(Constants.MessageLog.Insert, "Category", SerializeTool.Serialize(model.InfoCategory.MapTo<CategoryModel, Category>())));
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Add");
            }
        }
        #endregion
        #region #Edit
        public IActionResult Edit(int id)
        {
            var data = _categoryService.GetAll().ToList();
            data = data.FindAll(c => c.Id != id);
            if (data.Any() && data != null)
            {
                models.CategoriesParent.AddRange(data.Select(c => new SelectListItem()
                {
                    Text = c.Name,
                    Value = c.Id.ToString()
                }));
            }
            var categoryObj = _categoryService.FirstOrDefault(m => m.Id == id);
            if (categoryObj == null)
           
            models.InfoCategory = categoryObj.MapTo<Category, CategoryModel>();
            return View(models);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(CategoryViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    TempData[Constants.TempDataKey.Error] = Constants.Message.ValidateFail;
                    //return View(model);

                    return RedirectToAction("Index");
                }
                model.InfoCategory = model.InfoCategory.TrimStringProperties();
                var category = _categoryService.FirstOrDefault(c => c.Id == model.InfoCategory.Id);

                //category.Name = model.InfoCategory.Name;
                category.Title = model.InfoCategory.Title;
                category.SlugUrl = model.InfoCategory.SlugUrl;
                category.Description = model.InfoCategory.Description;
                category.Keywords = model.InfoCategory.Keywords;
                //category.OrderBy = model.InfoCategory.OrderBy;
                //category.ParentId = model.InfoCategory.ParentId;
                category.Status = model.InfoCategory.Status;
                //category.KeyName = model.InfoCategory.KeyName;
                category.UpdateDate = DateTime.Now;
                category.UpdatedBy = GetUserId(User.Identity);
                _categoryService.Edit(category);
                _categoryService.Save();
                TempData[Constants.TempDataKey.Success] = string.Format(Constants.Message.SuccessToUpdate, "Category");
                //Log
                AddActivityLog(this.HttpContext, Constants.LogType.Update, Constants.ResourceType.Admin,
                    string.Format(Constants.MessageLog.Update, "Category", SerializeTool.Serialize(category)));
                var url = "/administrator/category/index?type=" + model.InfoCategory.Type;
                return Redirect(url);
                //return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                FileUtils.LogFile(e);
                TempData[Constants.TempDataKey.Error] = string.Format(Constants.Message.FailToUpdate, "Category");
                return RedirectToAction("Index");
            }
        }
        #endregion
        #region #Delete
        public IActionResult Delete(int id)
        {
            try
            {
                var userObj = _categoryService.FirstOrDefault(c => c.Id == id);
                string logContent = string.Format(Constants.MessageLog.Delete, "Category", SerializeTool.Serialize(userObj));
                _categoryService.Delete(userObj);
                var data = _categoryService.FindAllBy(c => c.ParentId == id);
                if (data.Any())
                {
                    _categoryService.Delete(data);
                }
                _categoryService.Save();
                AddActivityLog(this.HttpContext, Constants.LogType.Delete, Constants.ResourceType.Admin, logContent);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                FileUtils.LogFile(e);
                return RedirectToAction("Index");
            }
        }
        public IActionResult DeleteMany(int?[] ids)
        {
            try
            {
                if (ids.Any())
                {
                    var cate = _categoryService.FindAllBy(c => ids.Contains(c.Id));
                    if (cate.Any())
                    {
                        _categoryService.Delete(cate);
                        _categoryService.Save();
                    }
                    //Delete childrenCate
                    var childrenCate = _categoryService.FindAllBy(c => ids.Contains(c.ParentId));
                    if (childrenCate.Any())
                    {
                        _categoryService.Delete(childrenCate);
                        _categoryService.Save();
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                FileUtils.LogFile(e);
                return RedirectToAction("Index");
            }

        }
        #endregion

        [HttpPost]
        public IActionResult CheckNameCreate(string name)
        {
            var data = _categoryService.FirstOrDefault(c => c.Name == name);
            if(data != null)
            {
                return Json(false);
            }
            else
            {
                return Json(true);
            }
        }
        public IActionResult CheckNameEdit(string name,int id)
        {
            var data = _categoryService.FirstOrDefault(c => c.Name == name && c.Id!=id);
            if (data != null)
            {
                return Json(false);
            }
            else
            {
                return Json(true);
            }
        }

        public IActionResult CategoryEditor(int id)
        {
            var data = _categoryService.GetAll().ToList();
            data = data.FindAll(c => c.Id != id);
            models.CategoriesParent.AddRange(data.Select(c => new SelectListItem()
            {
                Text = c.Name,
                Value = c.Id.ToString()
            }));

            var categoryObj = _categoryService.FirstOrDefault(m => m.Id == id);
            if (categoryObj == null) categoryObj = new Category();
            models.InfoCategory = categoryObj.MapTo<Category, CategoryModel>();
            return View(models);
        }

        [HttpPost]
        public IActionResult CategoryEditor(CategoryViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            model.InfoCategory.UpdatedBy = GetUserId(User.Identity);
            var save = _categoryService.SaveCategory(model.InfoCategory);
            if (!save.Success)
            {
                ModelState.AddModelError(save.Data + string.Empty, save.Message);
                return View(model);
            }
            TempData[Constants.TempDataKey.Success] = save.Message;
            AddActivityLog(this.HttpContext, (model.InfoCategory.Id == 0) ? Constants.LogType.Insert : Constants.LogType.Update, Constants.ResourceType.Admin, save.Message);
            return View(model);
        }
    }
}