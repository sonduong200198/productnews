﻿using Microsoft.AspNetCore.Mvc;
using BLOG.App_Core.Attributes;
using BLOG.Areas.Administrator.Controllers;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    [UserAuthorize]
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}