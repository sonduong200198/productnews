﻿using System.Linq;
using BLOG.App_Core.Attributes;
using BLOG.App_Core.Extensions;
using BLOG.Busin.ConstantProductType;
using BLOG.Business.Categories;
using BLOG.Business.ImagesProduct;
using BLOG.Business.Posts;
using BLOG.Business.Product;
using BLOG.Business.ProductType;
using BLOG.Core.Filters;
using BLOG.Models.Products;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using BLOG.Core.Constants;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using BLOG.Entities.Entities;
using BLOG.Models.ProductType;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System;
using BLOG.Busin.ProductCategory;

namespace BLOG.Areas.Administrator.Controllers
{
    [Area("Administrator")]
    [UserAuthorize]
    public class ProductController : BaseController
    {
        private readonly IProductService _productService;
        private readonly IProductTypeService _productTypeService;
        private readonly IConstantProductTypeService _constantProductTypeService;
        private readonly IImagesProductService _imagesProductService;
        private readonly ICategoryService _categoryService;
        private readonly ITagService _tagService;
        private readonly IProductCategoryService _productCategoryService;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment hostingEnviroment;
        ProductViewModel models = new ProductViewModel();
        public ProductController(IProductCategoryService productCategoryService, IHostingEnvironment hostingEnviroment, IProductService productService, IProductTypeService productTypeService, IConstantProductTypeService constantProductTypeService, IImagesProductService imagesProductService, ICategoryService categoryService, ITagService tagService, IMapper mapper)
        {
            _productService = productService;
            _productTypeService = productTypeService;
            _constantProductTypeService = constantProductTypeService;
            _imagesProductService = imagesProductService;
            _categoryService = categoryService;
            _tagService = tagService;
            _productCategoryService = productCategoryService;
            this.hostingEnviroment = hostingEnviroment;
            _mapper = mapper;
        }

        #region #Index
        public IActionResult Index(string sortBy, string orderBy, string keyword, int pageIndex = 1, int pageSize = 20, string type = "")
        {
            GlobalParamFilter filters = new GlobalParamFilter
            {
                OrderBy = orderBy,
                SortBy = sortBy,
                PageIndex = pageIndex,
                PageSize = pageSize > 0 ? pageSize : int.MaxValue,
                Keyword = keyword,
                Type = type
            };
            var products = _productService.GetAll(filters);
            if (products.Any())
            {
                models.ListProducts = products.Select(c => c.MapTo<Product, ProductModel>()).ToList();
            }
            models.keyWord = keyword;
            models.PageIndex = pageIndex;
            models.PageSize = pageSize;
            models.TotalPages = products.TotalPages;
            models.TotalCount = products.TotalCount;
            return View(models);
        }

        #endregion
        #region #Add
        public IActionResult Add()
        {
            var dataTags = _tagService.GetAll();

            if (dataTags.Any())
            {
                models.ListTags.AddRange(dataTags.Select(c => new SelectListItem()
                {
                    Value = c.Id.ToString(),
                    Text = c.Name
                }));
            }
            var dataCategories = _categoryService.FindAllBy(c => c.Type == Constants.CategoryType.Pro);
            if (dataCategories.Any())
            {
                models.ListCategories.AddRange(dataCategories.Select(c => new SelectListItem()
                {
                    Value = c.Id.ToString(),
                    Text = c.KeyName
                }));
            }
            var dataTypeProduct = _constantProductTypeService.GetAll();
            if (dataTypeProduct.Any())
            {
                models.ListSizeProduct.AddRange(dataTypeProduct.Select(c => new SelectListItem()
                {
                    Value = c.Id.ToString(),
                    Text = c.Size
                }));
                models.ListTypeProduct.AddRange(dataTypeProduct.Select(c => new SelectListItem()
                {
                    Value = c.Id.ToString(),
                    Text = c.Type
                }));
            }
            return View(models);
        }
        #endregion
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(ProductViewModel model, IFormFile fileImageCover, List<IFormFile> myFile)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                if (model.InfoProduct.ListTagChoosed != null)
                {
                    model.InfoProduct.Tags = string.Join(",", model.InfoProduct.ListTagChoosed);
                }
                var dataProduct = model.InfoProduct.MapTo<ProductModel, Product>();
                dataProduct.CreatedBy = GetUserId(User.Identity);
                if (fileImageCover != null)
                {
                    string uniqueFileName = null;

                    string path = hostingEnviroment.WebRootPath + "\\" + Constants.NameForderUploadImage.ForderImage;
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + fileImageCover.FileName;
                    string uploadsForder = Path.Combine(hostingEnviroment.WebRootPath, Constants.NameForderUploadImage.ForderImage);
                    string filePath = Path.Combine(uploadsForder, uniqueFileName);
                    fileImageCover.CopyTo(new FileStream(filePath, FileMode.Create));
                    if (!string.IsNullOrEmpty(uniqueFileName))
                        dataProduct.CoverImage = "/" + Constants.NameForderUploadImage.ForderImage + "/" + uniqueFileName;
                }
                _productService.Add(dataProduct);
                _productService.Save();
                var IdProductCurrent = _productService.FirstOrDefault(c => c.Name == model.InfoProduct.Name && c.ShortDescription == model.InfoProduct.ShortDescription).Id;
                _productService.Refresh(dataProduct);
                //Add categories
                if (model.InfoProduct.CateProductId.Any())
                {
                    List<ProductCategory> productCategories = new List<ProductCategory>();
                    var data = new ProductCategory();
                    for (var i = 0; i < model.InfoProduct.CateProductId.Count(); i++)
                    {
                        productCategories.Add(new ProductCategory()
                        {
                            CategoryId = model.InfoProduct.CateProductId[i],
                            ProductId = IdProductCurrent
                        });
                    }
                    _productCategoryService.Add(productCategories.AsQueryable());
                }
                if (model.ListProductTypeTable.Any())
                {
                    List<ProductType> listProductType = new List<ProductType>();
                    //List<ProductType> dataTypeProduct = data.ForEach(c => c.MapTo<ProductTypeModel, ProductType>());
                    //List<ProductType> dataTypeProduct = _mapper.Map<List<ProductType>>(data);
                    foreach (var item in model.ListProductTypeTable)
                    {
                        listProductType.Add(new ProductType()
                        {
                            Type = item.Type,
                            Size = item.Size,
                            Price = item.Price,
                            Promotion = item.Promotion,
                            ProductId = IdProductCurrent
                        });
                    }
                    _productTypeService.Add(listProductType.AsQueryable());

                }
                if (myFile.Any())
                {
                    List<ImagesProduct> listImages = new List<ImagesProduct>();
                    ImagesProduct images = new ImagesProduct();
                    foreach (var item in myFile)
                    {
                        string uniqueFileName = null;

                        string path = hostingEnviroment.WebRootPath + "\\" + Constants.NameForderUploadImage.ForderImage;
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        uniqueFileName = Guid.NewGuid().ToString() + "_" + item.FileName;
                        string uploadsForder = Path.Combine(hostingEnviroment.WebRootPath, Constants.NameForderUploadImage.ForderImage);
                        string filePath = Path.Combine(uploadsForder, uniqueFileName);
                        item.CopyTo(new FileStream(filePath, FileMode.Create));
                        if (!string.IsNullOrEmpty(uniqueFileName))
                            images.Image = "/" + Constants.NameForderUploadImage.ForderImage + "/" + uniqueFileName;
                        images.ProductId = IdProductCurrent;
                        listImages.Add(new ImagesProduct()
                        {
                            ProductId = IdProductCurrent,
                            Image = images.Image
                        });
                    }
                    _imagesProductService.Add(listImages.AsQueryable());
                }
                _productTypeService.Save();
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }
        #region #edit
        public IActionResult Edit(int id)
        {
            //get Product
            models.InfoProduct = _productService.FirstOrDefault(c => c.Id == id).MapTo<Product, ProductModel>();
            if (!String.IsNullOrEmpty(models.InfoProduct.Tags))
            {
                models.InfoProduct.ListTagChoosed = models.InfoProduct.Tags.Split(",".ToCharArray());
            }
            //Get list category choosed
            var cateProductChoosed = _productCategoryService.FindAllBy(c => c.ProductId == models.InfoProduct.Id);
            if (cateProductChoosed.Any() && cateProductChoosed != null)
            {
                models.InfoProduct.CateProductId = new List<int?>();
                foreach (var item in cateProductChoosed)
                {
                    models.InfoProduct.CateProductId.Add(item.CategoryId);
                }
            }
            //Get tag choosed and convert to array
            var dataTags = _tagService.GetAll();
            if (dataTags.Any())
            {
                models.ListTags.AddRange(dataTags.Select(c => new SelectListItem()
                {
                    Value = c.Id.ToString(),
                    Text = c.Name
                }));
            }
            var dataCategories = _categoryService.FindAllBy(c => c.Type == Constants.CategoryType.Pro);
            if (dataCategories.Any())
            {
                models.ListCategories.AddRange(dataCategories.Select(c => new SelectListItem()
                {
                    Value = c.Id.ToString(),
                    Text = c.KeyName
                }));
            }
            //Get constant typeproduct,size,promotion
            var dataTypeProduct = _constantProductTypeService.GetAll();
            if (dataTypeProduct.Any())
            {
                models.ListSizeProduct.AddRange(dataTypeProduct.Select(c => new SelectListItem()
                {
                    Value = c.Id.ToString(),
                    Text = c.Size
                }));
                models.ListTypeProduct.AddRange(dataTypeProduct.Select(c => new SelectListItem()
                {
                    Value = c.Id.ToString(),
                    Text = c.Type
                }));
            }
            //Get list typeproduct choosed
            var typeProductChoosed = _productTypeService.FindAllBy(c => c.ProductId == models.InfoProduct.Id);
            models.ListProductTypeTable = _mapper.Map<List<ProductTypeModel>>(typeProductChoosed);
            return View(models);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ProductViewModel model, IFormFile fileImageCover)
        {
            try
            {
                var dataProduct = _productService.FirstOrDefault(c => c.Id == model.InfoProduct.Id);
                dataProduct.Name = model.InfoProduct.Name;
                dataProduct.Title = model.InfoProduct.Title;
                dataProduct.Url = model.InfoProduct.Url;
                dataProduct.ShortDescription = model.InfoProduct.ShortDescription;
                dataProduct.LongDescription = model.InfoProduct.LongDescription;
                dataProduct.Keywords = model.InfoProduct.Keywords;
                dataProduct.Contents = model.InfoProduct.Contents;
                dataProduct.Tags = string.Join(",", model.InfoProduct.ListTagChoosed);
                string uniqueFileName = null;
                if (fileImageCover != null && fileImageCover.Length != 0)
                {
                    string path = hostingEnviroment.WebRootPath + "\\" + Constants.NameForderUploadImage.ForderImage;
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + fileImageCover.FileName;
                    string name = "/" + Constants.NameForderUploadImage.ForderImage + "/" + uniqueFileName;
                    if (name != dataProduct.CoverImage)
                    {

                        string uploadsForder = Path.Combine(hostingEnviroment.WebRootPath, Constants.NameForderUploadImage.ForderImage);
                        string filePath = Path.Combine(uploadsForder, uniqueFileName);
                        fileImageCover.CopyTo(new FileStream(filePath, FileMode.Create));
                        dataProduct.CoverImage = "/" + Constants.NameForderUploadImage.ForderImage + "/" + uniqueFileName;
                    }
                }
                _productService.Edit(dataProduct);
                //Update category
                var dataCate = _productCategoryService.FindAllBy(c => c.ProductId == model.InfoProduct.Id).AsQueryable();
                _productCategoryService.Delete(dataCate);
                //Update categories
                if (model.InfoProduct.CateProductId.Any())
                {
                    List<ProductCategory> productCategories = new List<ProductCategory>();
                    var data = new ProductCategory();
                    for (var i = 0; i < model.InfoProduct.CateProductId.Count(); i++)
                    {
                        productCategories.Add(new ProductCategory()
                        {
                            CategoryId = model.InfoProduct.CateProductId[i],
                            ProductId = model.InfoProduct.Id
                        });
                    }
                    _productCategoryService.Add(productCategories.AsQueryable());
                }
                //Update typeProduct
                var dataTypeProduct = _productTypeService.FindAllBy(c => c.ProductId == model.InfoProduct.Id).AsQueryable();
                _productTypeService.Delete(dataTypeProduct);
                if (model.ListProductTypeTable.Any())
                {
                    List<ProductType> listProductType = new List<ProductType>();
                    //List<ProductType> dataTypeProduct = data.ForEach(c => c.MapTo<ProductTypeModel, ProductType>());
                    //List<ProductType> dataTypeProduct = _mapper.Map<List<ProductType>>(data);
                    foreach (var item in model.ListProductTypeTable)
                    {
                        listProductType.Add(new ProductType()
                        {
                            Type = item.Type,
                            Size = item.Size,
                            Price = item.Price,
                            Promotion = item.Promotion,
                            ProductId = model.InfoProduct.Id
                        });
                    }
                    _productTypeService.Add(listProductType.AsQueryable());
                }
                _productService.Save();
                return RedirectToAction("Index");
        }
            catch
            {
                return View(model);
    }
}
        public IActionResult Delete(int id)
        {
            try
            {
                var dataCateProduct = _productCategoryService.FindAllBy(c => c.ProductId == id).AsQueryable();
                _productCategoryService.Delete(dataCateProduct);
                var dataProductType = _productTypeService.FindAllBy(c => c.ProductId == id).AsQueryable();
                _productTypeService.Delete(dataProductType);
                _productService.Delete(_productService.FirstOrDefault(c => c.Id == id));
                _productService.Save();
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }
        #endregion
    }

}