﻿using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Posts
{
    public partial interface IPostService : IGenericRepository<Post>
    {
        IPagedList<Post> GetAll(GlobalParamFilter filters);

    }
}
