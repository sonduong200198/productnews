﻿
using System;
using System.Collections.Generic;
using System.Linq;
using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using BLOG.Models.Posts;

namespace BLOG.Business.Posts
{
    public partial class TagService : GenericRepository<Tag>, ITagService
    {
        public TagService(BLOGContext dbContext) : base(dbContext)
        {

        }
        public IPagedList<Tag> GetAll(GlobalParamFilter filters)
        {
            var query = _context.Tag.AsQueryable();

            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.Name.Contains(filters.Keyword));
            // Sorted
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = query.ToList();

            // Return to list with paging
            return new PagedList<Tag>(results, filters.PageIndex, filters.PageSize);
        }


        private IQueryable<Tag> SortData(IQueryable<Tag> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Name) : query.OrderByDescending(c => c.Name);
                            break;
                        }
                   
                    case Constants.SortByData.CreateDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.CreateDate) : query.OrderByDescending(c => c.CreateDate);
                            break;
                        }
                    case Constants.SortByData.UpdatedDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.UpdateDate) : query.OrderByDescending(c => c.UpdateDate);
                            break;
                        }

                    default:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            
            return query;
        }

        public ResponseModel SaveTag(TagModel model)
        {
            var response = new ResponseModel();
            var entity = _context.Tag.FirstOrDefault(c => c.Id != model.Id && c.Name == model.Name);
            if(entity != null)
            {
                response.Success = false;
                response.Message = "The tag name is exists!";
                response.Data = "Name";
                return response;
            }
            entity = _context.Tag.FirstOrDefault(c => c.Id == model.Id);
            var currentTime = DateTime.Now;
            if(entity == null)
            {
                entity = new Tag();
                entity.CreateDate = currentTime;
                entity.CreatedBy = model.UpdatedBy;
                _context.Tag.Add(entity);
            }

            entity.Name = model.Name;

            entity.UpdateDate = currentTime;
            entity.UpdatedBy = model.UpdatedBy;
            _context.SaveChanges();

            response.Message = string.Format(Constants.Message.SuccessToUpdate, "Tag");
            if(model.Id == 0) response.Message = string.Format(Constants.Message.SuccessToCreate, "Tag");
            return response;
        }

        //AddModelError 
    }
}
