﻿using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using BLOG.Models.Posts;

namespace BLOG.Business.Posts
{
    public partial interface ITagService : IGenericRepository<Tag>
    {
        IPagedList<Tag> GetAll(GlobalParamFilter filters);
        
        ResponseModel SaveTag(TagModel model);
    }
}
