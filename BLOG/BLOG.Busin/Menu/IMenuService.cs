﻿using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Business.Menu
{
    public interface IMenuService : IGenericRepository<BLOG.Entities.Entities.Menu>
    {
        IPagedList<BLOG.Entities.Entities.Menu> GetAll(GlobalParamFilter filters);
    }
}
