﻿using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLOG.Business.ProductType
{
    public class ProductTypeService : GenericRepository<BLOG.Entities.Entities.ProductType>, IProductTypeService
    {
        public ProductTypeService(BLOGContext dbContext) : base(dbContext)
        {

        }
        public IPagedList<BLOG.Entities.Entities.ProductType> GetAll(GlobalParamFilter filters)
        {
            var query = _context.ProductType.AsQueryable();

            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.Size.Contains(filters.Keyword));
            // Sorted
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = query.ToList();

            // Return to list with paging
            return new PagedList<BLOG.Entities.Entities.ProductType>(results, filters.PageIndex, filters.PageSize);
        }


        private IQueryable<BLOG.Entities.Entities.ProductType> SortData(IQueryable<BLOG.Entities.Entities.ProductType> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Size) : query.OrderByDescending(c => c.Size);
                            break;
                        }

                    default:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            return query;
        }
    }
}

