﻿using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;


namespace BLOG.Business.ProductType
{
    public interface IProductTypeService : IGenericRepository<BLOG.Entities.Entities.ProductType>
    {
        IPagedList<BLOG.Entities.Entities.ProductType> GetAll(GlobalParamFilter filters);
    }
}
