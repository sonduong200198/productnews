﻿using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLOG.Business.Order
{
    public class OrderService : GenericRepository<BLOG.Entities.Entities.Order>, IOrderService
    {
        public OrderService(BLOGContext dbContext) : base(dbContext)
        {

        }
        public IPagedList<BLOG.Entities.Entities.Order> GetAll(GlobalParamFilter filters)
        {
            var query = _context.Order.AsQueryable();

            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.FirstName.Contains(filters.Keyword) || m.LastName.Contains(filters.Keyword) || m.Phone.Contains(filters.Keyword) || m.Email.Contains(filters.Keyword));
            // Sorted
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = query.ToList();

            // Return to list with paging
            return new PagedList<BLOG.Entities.Entities.Order>(results, filters.PageIndex, filters.PageSize);
        }


        private IQueryable<BLOG.Entities.Entities.Order> SortData(IQueryable<BLOG.Entities.Entities.Order> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.LastName) : query.OrderByDescending(c => c.LastName);
                            break;
                        }
                    case Constants.SortByData.CreateDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.CreateDate) : query.OrderByDescending(c => c.CreateDate);
                            break;
                        }
                    case Constants.SortByData.UpdatedDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.UpdateDate) : query.OrderByDescending(c => c.UpdateDate);
                            break;
                        }
                    default:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            return query;
        }
    }
}
