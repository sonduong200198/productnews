﻿using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Business.Order
{
    public interface IOrderService : IGenericRepository<BLOG.Entities.Entities.Order>
    {
        IPagedList<BLOG.Entities.Entities.Order> GetAll(GlobalParamFilter filters);
    }
}
