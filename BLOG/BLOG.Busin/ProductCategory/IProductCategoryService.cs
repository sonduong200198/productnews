﻿using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
namespace BLOG.Busin.ProductCategory
{
    public interface IProductCategoryService : IGenericRepository<BLOG.Entities.Entities.ProductCategory>
    {
        IPagedList<BLOG.Entities.Entities.ProductCategory> GetAll(GlobalParamFilter filters);
    }
}
