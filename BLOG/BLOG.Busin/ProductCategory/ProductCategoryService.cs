﻿using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLOG.Busin.ProductCategory
{
    public class ProductCategoryService : GenericRepository<BLOG.Entities.Entities.ProductCategory>, IProductCategoryService
    {
        public ProductCategoryService(BLOGContext dbContext) : base(dbContext)
        {

        }
        public IPagedList<BLOG.Entities.Entities.ProductCategory> GetAll(GlobalParamFilter filters)
        {
            var query = _context.ProductCategory.AsQueryable();

           
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = query.ToList();

            // Return to list with paging
            return new PagedList<BLOG.Entities.Entities.ProductCategory>(results, filters.PageIndex, filters.PageSize);
        }


        private IQueryable<BLOG.Entities.Entities.ProductCategory> SortData(IQueryable<BLOG.Entities.Entities.ProductCategory> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            return query;
        }
    }
}
