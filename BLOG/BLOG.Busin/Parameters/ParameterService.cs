﻿using System;
using System.Linq;
using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using BLOG.Models.Parameter;

namespace BLOG.Business.Parameters
{
    public partial class ParameterService : GenericRepository<Parameter>, IParameterService
    {
        public ParameterService(BLOGContext dbContext) : base(dbContext)
        {

        }
        public IPagedList<Parameter> GetAll(GlobalParamFilter filters)
        {
            var query = _context.Parameter.AsQueryable();
            if (!string.IsNullOrEmpty(filters.Type))
            {
                query = query.Where(c => c.Type == filters.Type);
            }
            //else
            //{
            //    query = query.Where(c => c.Type == null || c.Type==Constants.CategoryType.Branch);
            //}
            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.Name.Contains(filters.Keyword)|| m.Description.Contains(filters.Keyword) || m.Key.Contains(filters.Keyword));
            query = this.SortData(query, filters.SortBy, filters.OrderBy);
            var results = query.OrderBy(c=>c.Type).ToList();

            // Return to list with paging
            return new PagedList<Parameter>(results, filters.PageIndex, filters.PageSize);
        }

        private IQueryable<Parameter> SortData(IQueryable<Parameter> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Core.Constants.Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Core.Constants.Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Name) : query.OrderByDescending(c => c.Name);
                            break;
                        }

                    default:
                        {
                            query = orderBy.Equals(Core.Constants.Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            return query;
        }
        public ResponseModel SaveParameter(ParameterModel model)
        {
            var response = new ResponseModel();
            var entity = _context.Parameter.FirstOrDefault(c => c.Id != model.Id && c.Key == model.Key);
            if (entity != null)
            {
                response.Success = false;
                response.Message = "The Key is exists!";
                response.Data = "Key";
                return response;
            }
            entity = _context.Parameter.FirstOrDefault(c => c.Id == model.Id);
            var currentTime = DateTime.Now;
            if (entity == null)
            {
                entity = new Parameter();
                entity.CreatedDate = currentTime;
                entity.CreatedBy = model.UpdatedBy;
                _context.Parameter.Add(entity);
            }
            entity.Value = model.Value;
            entity.Name = model.Name;
            entity.Description = model.Description;

            entity.UpdatedDate = currentTime;
            entity.UpdatedBy = model.UpdatedBy;
            _context.SaveChanges();

            response.Message = string.Format(Constants.Message.SuccessToUpdate, "Parameter");
            if (model.Id == 0) response.Message = string.Format(Constants.Message.SuccessToCreate, "Parameter");
            return response;
        }
    }

}
