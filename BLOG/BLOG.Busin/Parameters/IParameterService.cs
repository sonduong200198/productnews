﻿using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using BLOG.Models.Parameter;

namespace BLOG.Business.Parameters
{
    public partial interface IParameterService : IGenericRepository<Parameter>
    {
        IPagedList<Parameter> GetAll(GlobalParamFilter filters);
        ResponseModel SaveParameter(ParameterModel model);
    }
}
