﻿using AutoMapper;
using BLOG.Business;
using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using BLOG.Models.ConstantProductType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLOG.Busin.ConstantProductType
{
    public class ConstantProductTypeService : GenericRepository<BLOG.Entities.Entities.ConstantProductType>, IConstantProductTypeService
    {
        private readonly IMapper _mapper;
        public ConstantProductTypeService(BLOGContext dbContext, IMapper mapper) : base(dbContext)
        {
            _mapper = mapper;

        }

        public IPagedList<Entities.Entities.ConstantProductType> GetAll(GlobalParamFilter filters)
        {
            var query = _context.ConstantProductType.AsQueryable();

            if (!string.IsNullOrEmpty(filters.Type))
            {
                query = query.Where(c => c.Type == filters.Type);
            }
            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.Type.Contains(filters.Keyword));

            // Sorted
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = query.ToList();

            // Return to list with paging
            return new PagedList<Entities.Entities.ConstantProductType>(results, filters.PageIndex, filters.PageSize);
        }

        private IQueryable<Entities.Entities.ConstantProductType> SortData(IQueryable<Entities.Entities.ConstantProductType> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Core.Constants.Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Core.Constants.Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Type) : query.OrderByDescending(c => c.Type);
                            break;
                        }
                    default:
                        {
                            query = orderBy.Equals(Core.Constants.Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            //else
            //{
            //    query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
            //        query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
            //}

            return query;
        }


        public ResponseModel SaveCategory(ConstantProductTypeModel model)
        {
            var response = new ResponseModel();

            var entity = _context.ConstantProductType.FirstOrDefault(c => c.Id == model.Id);
            var currentTime = DateTime.Now;
            if (entity == null)
            {
                entity = new BLOG.Entities.Entities.ConstantProductType();
                _context.ConstantProductType.Add(entity);
            }

            entity.Type = model.Type;
            entity.Size = model.Size;
            entity.Promotion = model.Promotion;
            entity.Color = model.Color;
            _context.SaveChanges();

            response.Message = string.Format(Constants.Message.SuccessToUpdate, "ConstantProductType");
            if (model.Id == 0) response.Message = string.Format(Constants.Message.SuccessToCreate, "ConstantProductType");
            return response;
        }
    }
}
