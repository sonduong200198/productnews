﻿using BLOG.Business;
using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Models.ConstantProductType;

namespace BLOG.Busin.ConstantProductType
{
    public interface IConstantProductTypeService : IGenericRepository<BLOG.Entities.Entities.ConstantProductType>
    {
        IPagedList<BLOG.Entities.Entities.ConstantProductType> GetAll(GlobalParamFilter filters);


        ResponseModel SaveCategory(ConstantProductTypeModel model);
    }
}

