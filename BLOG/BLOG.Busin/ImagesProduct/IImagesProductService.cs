﻿using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;

namespace BLOG.Business.ImagesProduct
{
    public interface IImagesProductService : IGenericRepository<BLOG.Entities.Entities.ImagesProduct>
    {
        IPagedList<BLOG.Entities.Entities.ImagesProduct> GetAll(GlobalParamFilter filters);
    }
}
