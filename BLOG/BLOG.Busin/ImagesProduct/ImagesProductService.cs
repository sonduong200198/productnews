﻿using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using BLOG.Models.Posts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLOG.Business.ImagesProduct
{
    public class ImagesProductService : GenericRepository<BLOG.Entities.Entities.ImagesProduct>, IImagesProductService
    {
        public ImagesProductService(BLOGContext dbContext) : base(dbContext)
        {

        }
        public IPagedList<BLOG.Entities.Entities.ImagesProduct> GetAll(GlobalParamFilter filters)
        {
            var query = _context.ImagesProduct.AsQueryable();

            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.Description.Contains(filters.Keyword));
            // Sorted
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = query.ToList();

            // Return to list with paging
            return new PagedList<BLOG.Entities.Entities.ImagesProduct>(results, filters.PageIndex, filters.PageSize);
        }


        private IQueryable<BLOG.Entities.Entities.ImagesProduct> SortData(IQueryable<BLOG.Entities.Entities.ImagesProduct> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Title) : query.OrderByDescending(c => c.Title);
                            break;
                        }

                    case Constants.SortByData.CreateDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.CreateDate) : query.OrderByDescending(c => c.CreateDate);
                            break;
                        }
                    case Constants.SortByData.UpdatedDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.UpdateDate) : query.OrderByDescending(c => c.UpdateDate);
                            break;
                        }

                    default:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }

            return query;
        }

    }
}
