﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Sys
{
    public partial class SysGroupFunctionService : GenericRepository<SysGroupFunction>, ISysGroupFunctionService
    {
        public SysGroupFunctionService(BLOGContext dbContext) : base(dbContext)
        {

        }

        public bool Delete(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var groupObj = _context.SysGroupFunction.Find(id);
                    if (groupObj != null)
                    {
                        var userGroupFunctions = _context.UserGroupFunction.Where(m => m.GroupFunctionId == id).AsQueryable();
                        if (userGroupFunctions.Any())
                        {
                            _context.UserGroupFunction.RemoveRange(userGroupFunctions);
                            _context.SaveChanges();
                        }
                        var functionGroupFunctions = _context.SysFunctionGroupFunction.Where(m => m.GroupFunctionId == id).AsQueryable();
                        if (functionGroupFunctions.Any())
                        {
                            _context.SysFunctionGroupFunction.RemoveRange(functionGroupFunctions);
                            _context.SaveChanges();
                        }

                        _context.SysGroupFunction.Remove(groupObj);
                        _context.SaveChanges();
                    }
                    else
                    {
                        return false;
                    }

                    transaction.Commit();
                    return true;
                }
                catch
                {
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public List<SysGroupFunction> GetByUser(int userId)
        {
            return (from gr in _context.SysGroupFunction
                join ugr in _context.UserGroupFunction on gr.Id equals ugr.GroupFunctionId
                where ugr.UserId == userId
                select gr).ToList();
        }

        public async Task<IPagedList<SysGroupFunction>> GetAll(GlobalParamFilter filters)
        {
            var query = _context.SysGroupFunction.AsQueryable();

            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.Name.Contains(filters.Keyword) || m.Description.Contains(filters.Keyword));
            // Sorted
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = await query.ToListAsync();

            // Return to list with paging
            return new PagedList<SysGroupFunction>(results, filters.PageIndex, filters.PageSize);
        }

        private IQueryable<SysGroupFunction> SortData(IQueryable<SysGroupFunction> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Name) : query.OrderByDescending(c => c.Name);
                            break;
                        }
                    case Constants.SortByData.CreateDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.CreatedDate) : query.OrderByDescending(c => c.CreatedDate);
                            break;
                        }
                    case Constants.SortByData.UpdatedDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.UpdatedDate) : query.OrderByDescending(c => c.UpdatedDate);
                            break;
                        }

                    default:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            else
            {
                query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                    query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
            }

            return query;
        }
    }
}
