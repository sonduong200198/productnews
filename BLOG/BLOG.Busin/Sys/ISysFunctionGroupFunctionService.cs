﻿using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Sys
{
    public partial interface ISysFunctionGroupFunctionService : IGenericRepository<SysFunctionGroupFunction>
    {

    }
}
