﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using BLOG.Models.Sys;

namespace BLOG.Business.Sys
{
    public partial class SysMenuGroupService : GenericRepository<SysMenuGroup>, ISysMenuGroupService
    {
        public SysMenuGroupService(BLOGContext dbContext) : base(dbContext)
        {

        }

        public List<MenuPermissionModel> GetMenuGroupAll()
        {
            var query = _context.SysMenuGroup.Select(c => new MenuPermissionModel
            {
                Id = c.Id,
                Name = c.Name,
                Icon = c.Icon
            }).Distinct().ToList();
            return query;
        }

        public List<MenuPermissionModel> GetMenuGroupByIds(List<int> ids)
        {
            var query = _context.SysMenuGroup.Where(m => ids.Contains(m.Id)).Select(c=> new MenuPermissionModel
            {
                Id = c.Id,
                Name = c.Name,
                Icon = c.Icon
            }).Distinct().ToList();
            return query;
        }
        public async Task<IPagedList<SysMenuGroup>> GetAll(GlobalParamFilter filters)
        {
            var query = _context.SysMenuGroup.AsQueryable();

            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.Name.Contains(filters.Keyword) || m.Description.Contains(filters.Keyword));
            // Sorted
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = await query.ToListAsync();

            // Return to list with paging
            return new PagedList<SysMenuGroup>(results, filters.PageIndex, filters.PageSize);
        }

        private IQueryable<SysMenuGroup> SortData(IQueryable<SysMenuGroup> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Name) : query.OrderByDescending(c => c.Name);
                            break;
                        }
                    case Constants.SortByData.CreateDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.CreatedDate) : query.OrderByDescending(c => c.CreatedDate);
                            break;
                        }
                    case Constants.SortByData.UpdatedDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.UpdatedDate) : query.OrderByDescending(c => c.UpdatedDate);
                            break;
                        }

                    default:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            else
            {
                query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                    query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
            }

            return query;
        }
    }
}
