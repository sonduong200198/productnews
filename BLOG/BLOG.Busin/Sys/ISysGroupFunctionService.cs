﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Sys
{
    public partial interface ISysGroupFunctionService : IGenericRepository<SysGroupFunction>
    {
        bool Delete(int id);

        List<SysGroupFunction> GetByUser(int userId);

        Task<IPagedList<SysGroupFunction>> GetAll(GlobalParamFilter filters);
    }
}
