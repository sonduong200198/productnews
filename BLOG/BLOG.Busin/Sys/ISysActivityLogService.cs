﻿using System.Threading.Tasks;
using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Sys
{
    public partial interface ISysActivityLogService : IGenericRepository<SysActivityLog>
    {
        Task<IPagedList<SysActivityLog>> GetAll(GlobalParamFilter filters);
    }
}
