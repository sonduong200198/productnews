﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using BLOG.Models.Sys;

namespace BLOG.Business.Sys
{
    public partial interface ISysMenuGroupService : IGenericRepository<SysMenuGroup>
    {
        List<MenuPermissionModel> GetMenuGroupAll();
        List<MenuPermissionModel> GetMenuGroupByIds(List<int> ids);
        Task<IPagedList<SysMenuGroup>> GetAll(GlobalParamFilter filters);
    }
}
