﻿using BLOG.Entities;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Sys
{
    public partial class SysFunctionGroupFunctionService : GenericRepository<SysFunctionGroupFunction>, ISysFunctionGroupFunctionService
    {
        public SysFunctionGroupFunctionService(BLOGContext dbContext) : base(dbContext)
        {

        }
    }
}
