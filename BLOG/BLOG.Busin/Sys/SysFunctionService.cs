﻿using System.Collections.Generic;
using System.Linq;
using BLOG.Entities;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using BLOG.Models.Sys;
using BLOG.Business;

namespace BLOG.Business.Sys
{
    public partial class SysFunctionsService : GenericRepository<SysFunction>, ISysFunctionsService
    {
        public SysFunctionsService(BLOGContext dbContext) : base(dbContext)
        {

        }

        public bool Delete(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var functionObj = _context.SysFunction.Find(id);
                    if (functionObj != null)
                    {
                        var userFunctions = _context.UserFunction.Where(m => m.FunctionCode == functionObj.Key).AsQueryable();
                        if (userFunctions.Any())
                        {
                            _context.UserFunction.RemoveRange(userFunctions);
                            _context.SaveChanges();
                        }
                        var functionGroupFunctions = _context.SysFunctionGroupFunction.Where(m => m.FunctionId == id).AsQueryable();
                        if (functionGroupFunctions.Any())
                        {
                            _context.SysFunctionGroupFunction.RemoveRange(functionGroupFunctions);
                            _context.SaveChanges();
                        }

                        _context.SysFunction.Remove(functionObj);
                        _context.SaveChanges();
                    }
                    else
                    {
                        return false;
                    }

                    transaction.Commit();
                    return true;
                }
                catch
                {
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public ResponseModel UpdatePermissions(List<SysGroupFunctionModel> groupItems)
        {
            ResponseModel result = new ResponseModel();
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    foreach (var item in groupItems)
                    {
                        //Delete function group funcion old
                        var functionGroupFunctions = _context.SysFunctionGroupFunction.Where(m => m.GroupFunctionId == item.Id).AsQueryable();
                        if (functionGroupFunctions.Any())
                        {
                            _context.SysFunctionGroupFunction.RemoveRange(functionGroupFunctions);
                            _context.SaveChanges();
                        }
                        if (item.FunctionIds.Any())
                        {
                            var permissions = item.FunctionIds.Select(c => new SysFunctionGroupFunction
                            {
                                FunctionId= c,
                                GroupFunctionId =item.Id
                            }).ToList();
                            if (permissions.Any())
                            {
                                _context.SysFunctionGroupFunction.AddRange(permissions);
                                _context.SaveChanges();
                            }
                        }
                    }

                    transaction.Commit();
                    result.Success = true;
                }
                catch
                {
                    transaction.Rollback();
                    result.Success = false;
                }
            }

            return result;
        }
    }
}
