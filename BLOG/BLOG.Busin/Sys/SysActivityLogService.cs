﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Sys
{
    public partial class SysActivityLogService : GenericRepository<SysActivityLog>, ISysActivityLogService
    {
        public SysActivityLogService(BLOGContext dbContext) : base(dbContext)
        {

        }

        public async Task<IPagedList<SysActivityLog>> GetAll(GlobalParamFilter filters)
        {
            var query = _context.SysActivityLog.AsQueryable();
            if (filters.StartDate.HasValue)
                query = query.Where(m => m.CreateDate >= filters.StartDate.Value);
            if (filters.EndDate.HasValue)
                query = query.Where(m => m.CreateDate <= filters.EndDate.Value);
            if (filters.UserId.HasValue)
                query = query.Where(m => m.UserId == filters.UserId);
            if (!string.IsNullOrEmpty(filters.Type))
                query = query.Where(m => m.LogType == filters.Type);
            
            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.Comment.Contains(filters.Keyword));

            // Sorted
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = await query.ToListAsync();
            // Return to list with paging
            return new PagedList<SysActivityLog>(results, filters.PageIndex, filters.PageSize);
        }

        private IQueryable<SysActivityLog> SortData(IQueryable<SysActivityLog> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.LogType) : query.OrderByDescending(c => c.LogType);
                            break;
                        }
                    
                    case Constants.SortByData.CreateDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.CreateDate) : query.OrderByDescending(c => c.CreateDate);
                            break;
                        }
                    
                    default:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            else
            {
                query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                    query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
            }

            return query;
        }
    }
}
