﻿using System.Collections.Generic;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using BLOG.Models.Sys;
using BLOG.Business;

namespace BLOG.Business.Sys
{
    public partial interface ISysFunctionsService : IGenericRepository<SysFunction>
    {
        bool Delete(int id);

        ResponseModel UpdatePermissions(List<SysGroupFunctionModel> groupItems);
    }
}
