﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BLOG.Business.Users;
using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Users
{
    public partial class UserService : GenericRepository<User>, IUserService
    {
        public UserService(BLOGContext dbContext) : base(dbContext)
        {

        }

        public bool Delete(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var userObj = _context.User.Find(id);
                    if (userObj!=null)
                    {
                        var userGroupFunctions = _context.UserGroupFunction.Where(m => m.UserId == id).AsQueryable();
                        if (userGroupFunctions.Any())
                        {
                            _context.UserGroupFunction.RemoveRange(userGroupFunctions);
                            _context.SaveChanges();
                        }
                        var userFunctions = _context.UserFunction.Where(m => m.UserId == id).AsQueryable();
                        if (userFunctions.Any())
                        {
                            _context.UserFunction.RemoveRange(userFunctions);
                            _context.SaveChanges();
                        }

                        _context.User.Remove(userObj);
                        _context.SaveChanges();
                    }
                    else
                    {
                        return false;
                    }

                    transaction.Commit();
                    return true;
                }
                catch
                {
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public List<SysFunction> GetPermissionsByUser(string userName)
        {
            List<SysFunction> result = new List<SysFunction>();
            var userObj = _context.User.FirstOrDefault(m => m.UserName == userName);
            if (userObj == null)
                return result;

            var functions = (from fc in _context.SysFunction
                join fgfc in _context.SysFunctionGroupFunction on fc.Id equals fgfc.FunctionId
                join ugf in _context.UserGroupFunction on fgfc.GroupFunctionId equals ugf.GroupFunctionId
                where ugf.UserId == userObj.Id
                select fc).Distinct().ToList();
            return functions;
        }
        public async Task<IPagedList<User>> GetAll(GlobalParamFilter filters)
        {
            var query = _context.User.Where(m => m.UserName.ToLower() != Constants.AdminUserName.ToLower()).AsQueryable();
            if (filters.Status.HasValue)
                query = query.Where(m => m.Status == filters.Status.Value);

            if (filters.GroupId > 0)
            {
                query = (from a in query
                    join ugf in _context.UserGroupFunction on a.Id equals ugf.UserId
                    where ugf.GroupFunctionId == filters.GroupId
                    select a);
            }
            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.Email.Contains(filters.Keyword) || m.FirstName.Contains(filters.Keyword) || m.LastName.Contains(filters.Keyword));
            
            // Sorted
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = await query.ToListAsync();

            // Return to list with paging
            return new PagedList<User>(results, filters.PageIndex, filters.PageSize);
        }

        private IQueryable<User> SortData(IQueryable<User> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.UserName) : query.OrderByDescending(c => c.UserName);
                            break;
                        }
                    case Constants.SortByData.Status:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Status) : query.OrderByDescending(c => c.Status);
                            break;
                        }
                    case Constants.SortByData.CreateDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.CreatedDate) : query.OrderByDescending(c => c.CreatedDate);
                            break;
                        }
                    case Constants.SortByData.UpdatedDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.UpdatedDate) : query.OrderByDescending(c => c.UpdatedDate);
                            break;
                        }

                    default:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            else
            {
                query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                    query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
            }

            return query;
        }
    }
}
