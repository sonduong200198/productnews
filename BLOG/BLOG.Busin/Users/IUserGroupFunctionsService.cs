﻿using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Users
{
    public partial interface IUserGroupFunctionsService : IGenericRepository<UserGroupFunction>
    {
        
    }
}
