﻿using BLOG.Business.Users;
using BLOG.Entities;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Users
{
    public partial class UserGroupFunctionsService : GenericRepository<UserGroupFunction>, IUserGroupFunctionsService
    {
        public UserGroupFunctionsService(BLOGContext dbContext) : base(dbContext)
        {

        }
    }
}
