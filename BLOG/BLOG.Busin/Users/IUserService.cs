﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Users
{
    public partial interface IUserService : IGenericRepository<User>
    {
        bool Delete(int id);

        List<SysFunction> GetPermissionsByUser(string userName);

        Task<IPagedList<User>> GetAll(GlobalParamFilter filters);
    }
}
