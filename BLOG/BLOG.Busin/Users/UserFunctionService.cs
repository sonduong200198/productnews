﻿using BLOG.Business.Users;
using BLOG.Entities;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;

namespace BLOG.Business.Users
{
    public partial class UserFunctionService : GenericRepository<UserFunction>, IUserFunctionService
    {
        public UserFunctionService(BLOGContext dbContext) : base(dbContext)
        {

        }
    }
}
