﻿using AutoMapper;
using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using System;
using System.Linq;
using BLOG.Core.Constants;
using BLOG.Models.Categories;

namespace BLOG.Business.Categories
{
    public partial class CategoryService : GenericRepository<Category>, ICategoryService
    {
        private readonly IMapper _mapper;
        public CategoryService(BLOGContext dbContext, IMapper mapper) : base(dbContext)
        {
            _mapper = mapper;

        }
      
        public IPagedList<Category> GetAll(GlobalParamFilter filters)
        {
            var query = _context.Category.AsQueryable();
            
            if (!string.IsNullOrEmpty(filters.Type))
            {
                query = query.Where(c => c.Type == filters.Type);
            }
            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.Name.Contains(filters.Keyword) || m.Title.Contains(filters.Keyword) || m.Description.Contains(filters.Keyword));

            // Sorted
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = query.ToList();

            // Return to list with paging
            return new PagedList<Category>(results, filters.PageIndex, filters.PageSize);
        }

        private IQueryable<Category> SortData(IQueryable<Category> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Core.Constants.Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Core.Constants.Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Name) : query.OrderByDescending(c => c.Name);
                            break;
                        }
                    case Core.Constants.Constants.SortByData.Status:
                        {
                            query = orderBy.Equals(Core.Constants.Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Status) : query.OrderByDescending(c => c.Status);
                            break;
                        }
                    case Core.Constants.Constants.SortByData.CreateDate:
                        {
                            query = orderBy.Equals(Core.Constants.Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.CreateDate) : query.OrderByDescending(c => c.CreateDate);
                            break;
                        }
                    case Core.Constants.Constants.SortByData.UpdatedDate:
                        {
                            query = orderBy.Equals(Core.Constants.Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.UpdateDate) : query.OrderByDescending(c => c.UpdateDate);
                            break;
                        }

                    default:
                        {
                            query = orderBy.Equals(Core.Constants.Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            //else
            //{
            //    query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
            //        query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
            //}

            return query;
        }

        
        public ResponseModel SaveCategory(CategoryModel model)
        {
            var response = new ResponseModel();

            var entity = _context.Category.FirstOrDefault(c => c.Id == model.Id);
            var currentTime = DateTime.Now;
            if (entity == null)
            {
                entity = new Category();
                entity.CreateDate = currentTime;
                entity.CreatedBy = model.UpdatedBy;
                _context.Category.Add(entity);
            }

            entity.Title = model.Title;
            entity.SlugUrl = model.SlugUrl;
            entity.Keywords = model.Keywords;
            entity.KeyName = model.KeyName;
            entity.Description = model.Description;
            entity.Status = model.Status;
            if (model.Type != null)
                entity.Type = model.Type;
            entity.UpdateDate = currentTime;
            entity.UpdatedBy = model.UpdatedBy;
            _context.SaveChanges();

            response.Message = string.Format(Constants.Message.SuccessToUpdate, "Category");
            if (model.Id == 0) response.Message = string.Format(Constants.Message.SuccessToCreate, "Category");
            return response;
        }
    }
}
