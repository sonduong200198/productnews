﻿using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using BLOG.Models.Categories;

namespace BLOG.Business.Categories
{
    public partial interface ICategoryService : IGenericRepository<Category>
    {
        IPagedList<Category> GetAll(GlobalParamFilter filters);
        

        ResponseModel SaveCategory(CategoryModel model);
    }
}
