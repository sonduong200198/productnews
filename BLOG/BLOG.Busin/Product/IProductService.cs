﻿using BLOG.Core;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLOG.Business.Product
{
    public interface IProductService : IGenericRepository<BLOG.Entities.Entities.Product>
    {
        IPagedList<BLOG.Entities.Entities.Product> GetAll(GlobalParamFilter filters);
    }
}
