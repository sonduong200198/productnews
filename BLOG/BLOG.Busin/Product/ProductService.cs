﻿using BLOG.Core;
using BLOG.Core.Constants;
using BLOG.Core.Filters;
using BLOG.Entities.Base;
using BLOG.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLOG.Business.Product
{
    public class ProductService : GenericRepository<BLOG.Entities.Entities.Product>, IProductService
    {
        public ProductService(BLOGContext dbContext) : base(dbContext)
        {

        }
        
        public IPagedList<BLOG.Entities.Entities.Product> GetAll(GlobalParamFilter filters)
        {
            var query = _context.Product.AsQueryable();
            
            if (!string.IsNullOrEmpty(filters.Keyword))
                query = query.Where(m => m.Name.Contains(filters.Keyword) || m.LongDescription.Contains(filters.Keyword) || m.ShortDescription.Contains(filters.Keyword) || m.Title.Contains(filters.Keyword));
            // Sorted
            query = this.SortData(query, filters.SortBy, filters.OrderBy);

            var results = query.ToList();

            // Return to list with paging
            return new PagedList<BLOG.Entities.Entities.Product>(results, filters.PageIndex, filters.PageSize);
        }


        private IQueryable<BLOG.Entities.Entities.Product> SortData(IQueryable<BLOG.Entities.Entities.Product> query, string sortBy, string orderBy)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy.ToUpper())
                {
                    case Constants.SortByData.Name:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Name) : query.OrderByDescending(c => c.Name);
                            break;
                        }
                    case Constants.SortByData.CreateDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.CreateDate) : query.OrderByDescending(c => c.CreateDate);
                            break;
                        }
                    case Constants.SortByData.UpdatedDate:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.UpdateDate) : query.OrderByDescending(c => c.UpdateDate);
                            break;
                        }
                    default:
                        {
                            query = orderBy.Equals(Constants.OrderByData.Asc, StringComparison.OrdinalIgnoreCase) ?
                                query.OrderBy(c => c.Id) : query.OrderByDescending(c => c.Id);
                            break;
                        }
                }
            }
            return query;
        }
    }
}
